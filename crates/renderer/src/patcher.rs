use hotwatch::{notify::Event, Hotwatch};
use marpii::{resources::ShaderModule, OoS};
use patch_link::{FuncIdent, MakeFunctionImport};
use spv_patcher::rspirv::{
    self,
    binary::Assemble,
    dr::{Instruction, Module, Operand},
    spirv::{AddressingModel, MemoryModel, Op},
};
use std::{
    path::{Path, PathBuf},
    sync::{
        mpsc::{Receiver, TryRecvError},
        Arc,
    },
    time::{Duration, Instant},
};

///Hotwatch local patcher that first executes msdfc, then patches that into the
/// base shader.
struct LivePatcher {}

///Patcher utility. Observes the `sdf.vola` file, and recompiles the shader if needed.
pub struct Patcher {
    ///Receiver of new shader modules
    recv: Receiver<OoS<ShaderModule>>,
    hotwatch: Hotwatch,
}

struct Linker {
    src_file: PathBuf,
    //Dirty suffix thingy so the patchers don't interfere
    suffix: String,
}

impl Drop for Linker {
    fn drop(&mut self) {
        // let _ = std::fs::remove_file(format!("patchcode_{}.spv", self.suffix));
        // let _ = std::fs::remove_file(format!("basecode_{}.spv", self.suffix));
        // let _ = std::fs::remove_file(format!("outfile_{}.spv", self.suffix));
    }
}

impl Linker {
    pub fn new(module: &Module, suffix: &str) -> Self {
        let mut module = module.clone();
        // make version 1.5 _always_
        module.header.as_mut().unwrap().set_version(1, 5);
        module.memory_model = Some(Instruction::new(
            Op::MemoryModel,
            None,
            None,
            vec![
                Operand::AddressingModel(AddressingModel::Logical),
                Operand::MemoryModel(MemoryModel::Vulkan),
            ],
        ));

        let src_file = PathBuf::from(format!("basecode_{}.spv", suffix));
        Self::write_module_to_file(&src_file, &module);
        Self {
            src_file,
            suffix: suffix.to_owned(),
        }
    }

    fn write_module_to_file(file: &dyn AsRef<Path>, module: &Module) {
        let code = module.assemble();
        std::fs::write(file, bytemuck::cast_slice(&code)).unwrap();
    }

    ///Links `file` into `self.src_file`. If successful, returns the newly
    ///created module
    pub fn link_new(&self, module: &Vec<u8>) -> Option<Module> {
        std::fs::write(
            format!("patchcode_{}.spv", self.suffix),
            bytemuck::cast_slice(module.as_slice()),
        )
        .unwrap();
        //call the linker
        match std::process::Command::new("spirv-link")
            .arg("-o")
            .arg(format!("outfile_{}.spv", self.suffix))
            .arg(&self.src_file)
            .arg(format!("patchcode_{}.spv", self.suffix))
            .output()
        {
            Ok(_) => {
                let codebuffer = std::fs::read(format!("outfile_{}.spv", self.suffix))
                    .expect(&format!("No file outfile_{}.spv found", self.suffix));
                let module = rspirv::dr::load_bytes(&codebuffer).unwrap();
                Some(module)
            }
            Err(e) => {
                log::error!("Failed to link module: {e:?}");
                None
            }
        }
    }
}

impl Patcher {
    pub fn new(
        device: Arc<marpii::context::Device>,
        base_code: Vec<u8>,
        suffix: &str,
        links: &[&str],
    ) -> Self {
        let (send, recv) = std::sync::mpsc::channel();

        //NOTE: PreSend the first shader module, by just loading the base shader without patches.
        let base_shader = ShaderModule::new_from_bytes(&device, &base_code)
            .expect("Could not build base-shader module!");

        send.send(OoS::new(base_shader))
            .expect("Failed to send initial base shader!");

        let mut watcher = Hotwatch::new_with_custom_delay(Duration::from_millis(500))
            .expect("Could not create file watcher!");

        if !Path::new("../volasrc/sdf.vola").exists() {
            panic!("File sdf.vola does not exist!");
        }

        //touch the file

        let patcher = spv_patcher::Module::new(base_code).expect("Could not load basecode");
        //prepare the base shader to be linked at some point

        //Try to make all known entry points patchable
        let mut linkabel_module = patcher.patch();
        for link in links.iter() {
            linkabel_module = linkabel_module
                .patch(MakeFunctionImport {
                    name: link.to_string(),
                    identify_by: FuncIdent::Name(link.to_string()),
                })
                .expect(&format!(
                    "Could not make entry point {suffix}::{link} patchable!"
                ));
        }

        //write the module to a file that will be read by at link-time
        let linker = Linker::new(&linkabel_module.unwrap_module(), suffix);

        let cpipeline = volac::Pipeline {
            target_format: volac::Backend::Spirv,
            target: volac::Target::buffer(),
            early_cnf: true,
            late_cne: true,
            late_cnf: true,
        };

        let file_path = std::path::PathBuf::from("../volasrc/sdf.vola");

        watcher
            .watch("../volasrc/", move |ev: Event| {
                let start = Instant::now();
                match std::panic::catch_unwind(|| {
                    if !ev.kind.is_modify() {
                        return;
                    }

                    //First of all, try to run the compiler
                    let module = match cpipeline.execute_on_file(&file_path) {
                        Ok(t) => match t {
                            volac::Target::Buffer(b) => b,
                            volac::Target::File(f) => std::fs::read(f).unwrap(),
                        },
                        Err(e) => {
                            log::error!("Failed to compile sdf.vola: {e}");
                            return;
                        }
                    };

                    //now, try to inject all fields, based on the name using the patcher for our base module
                    let mut patch = patcher.patch();
                    println!("Injecting module {}", linker.suffix);

                    let linked_module = if let Some(m) = linker.link_new(&module) {
                        m
                    } else {
                        log::error!("Failed to link module!");
                        return;
                    };

                    let new_module_code = linked_module.assemble();

                    match ShaderModule::new_from_bytes(
                        &device,
                        bytemuck::cast_slice(&new_module_code),
                    ) {
                        Ok(sm) => {
                            let _ = send.send(sm.into());
                        }
                        Err(e) => {
                            log::error!("Could not build shader module for patched shader: {e}")
                        }
                    }
                }) {
                    Ok(_) => {
                        println!(
                            "Successfuly patched {} in {}ms",
                            linker.suffix,
                            start.elapsed().as_secs_f32() * 1000.0
                        );
                    }
                    Err(e) => println!("Patching failed with panic"),
                }
            })
            .expect("Could not schedule sdf-file watcher!");

        Self {
            recv,
            hotwatch: watcher,
        }
    }

    pub fn fetch_new_module(&mut self) -> Option<OoS<ShaderModule>> {
        match self.recv.try_recv() {
            Ok(new) => Some(new),
            Err(TryRecvError::Disconnected) => {
                log::error!("Patch receiver is disconnected, restart the application!");
                None
            }
            _ => None,
        }
    }
}
