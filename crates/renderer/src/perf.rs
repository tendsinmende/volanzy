use ahash::AHashMap;
use fixring::RingBuffer;

///Performance information
#[derive(Clone)]
pub struct Perf {
    //Per pass data
    pub sets: AHashMap<String, RingBuffer<(u32, f32), 128>>,
}

impl Perf {
    pub fn new() -> Self {
        Perf {
            sets: AHashMap::default(),
        }
    }

    pub fn into_plot<'a>(&'a self) -> Vec<egui_widgets::PlotItem<'a>> {
        let mut vec = Vec::with_capacity(self.sets.len());

        for (name, buf) in self.sets.iter() {
            vec.push((name, buf));
        }

        vec
    }
}
