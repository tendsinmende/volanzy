use marpii::{
    ash::vk,
    resources::{ImageType, ImgDesc, SharingMode},
};
use marpii_rmg::{ImageHandle, Rmg, SamplerHandle};
use marpii_rmg_tasks::UploadImage;

use super::bluenoise::blue_noise_raw_reader;

pub struct AuxData {
    pub bluenoise_rgba: ImageHandle,
    pub bluenoise_extent: vk::Extent3D,
    pub linear_sampler_clamp: SamplerHandle,
}

impl AuxData {
    pub fn new(rmg: &mut Rmg) -> Self {
        let (bluenoise, bluenoise_extent) = {
            let (bluenoise_buffer, extent, format) = blue_noise_raw_reader();
            let img_desc = ImgDesc {
                img_type: ImageType::Tex3d,
                format,
                extent,
                mip_levels: 1,
                samples: vk::SampleCountFlags::TYPE_1,
                tiling: vk::ImageTiling::OPTIMAL,
                usage: vk::ImageUsageFlags::SAMPLED,
                sharing_mode: SharingMode::Exclusive,
                ..Default::default()
            };
            let mut upload_pass = UploadImage::new(rmg, &bluenoise_buffer, img_desc)
                .expect("Failed to upload bluenoise");

            //upload
            rmg.record()
                .add_task(&mut upload_pass)
                .expect("Could not submit BlueNoise upload")
                .execute()
                .expect("Failed to execute BlueNoise upload");

            (upload_pass.image.clone(), extent)
        };
        let lut_sampler_nearest = rmg
            .new_sampler(
                &vk::SamplerCreateInfo::default()
                    .address_mode_u(vk::SamplerAddressMode::CLAMP_TO_EDGE)
                    .address_mode_v(vk::SamplerAddressMode::CLAMP_TO_EDGE)
                    .address_mode_w(vk::SamplerAddressMode::CLAMP_TO_EDGE)
                    .mag_filter(vk::Filter::NEAREST)
                    .min_filter(vk::Filter::NEAREST),
            )
            .unwrap();

        AuxData {
            bluenoise_rgba: bluenoise,
            bluenoise_extent,
            linear_sampler_clamp: lut_sampler_nearest,
        }
    }
}
