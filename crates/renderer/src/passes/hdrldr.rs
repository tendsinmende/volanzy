use crate::{
    passes::{hdr::Hdr, primary::Primary},
    patcher::Patcher,
    Camera,
};
use marpii::{
    ash::vk::{self, Extent2D, ImageUsageFlags},
    resources::{
        ComputePipeline, ImgDesc, PipelineLayout, PushConstant, ShaderModule, ShaderStage,
    },
    OoS,
};
use marpii_rmg::{CtxRmg, ImageHandle, Rmg, Task};
use marpii_rmg_tasks::SwapchainPresent;
use shared::{glam::Vec3, RenderUniform};
use std::sync::Arc;

static HDRLDR_SHADER: &'static [u8] = include_bytes!("../../../../assets/spirv/shader-hdr-ldr.spv");
pub struct HdrLdr {
    pipeline_layout: Arc<PipelineLayout>,
    pipeline: Arc<ComputePipeline>,
    pub target_image: ImageHandle,
    pc: PushConstant<shared::HdrToLdrPush>,
    hdr_image: ImageHandle,
    depth_image: ImageHandle,
}

impl HdrLdr {
    pub fn new(rmg: &mut Rmg, primary: &Primary, hdr: &Hdr) -> Self {
        let resolution = primary.target_image.extent_2d();
        let module = ShaderModule::new_from_bytes(&rmg.ctx.device, HDRLDR_SHADER)
            .expect("Could not load hdr-ldr shader");
        let pc = PushConstant::new(
            shared::HdrToLdrPush::default(),
            vk::ShaderStageFlags::COMPUTE,
        );
        let shader_stage = ShaderStage::from_module(
            module.into(),
            vk::ShaderStageFlags::COMPUTE,
            "hdrldr".to_owned(),
        );
        let pipeline = Arc::new(
            ComputePipeline::new(
                &rmg.ctx.device,
                &shader_stage,
                None,
                rmg.resources.bindless_layout(),
            )
            .unwrap(),
        );

        let target_image = rmg
            .new_image_uninitialized(
                ImgDesc::storage_image_2d(
                    resolution.width,
                    resolution.height,
                    vk::Format::R8G8B8A8_UNORM,
                )
                .add_usage(ImageUsageFlags::COLOR_ATTACHMENT),
                Some("hdr-ldr-target"),
            )
            .unwrap();

        HdrLdr {
            target_image,
            pc,
            pipeline_layout: rmg.resources.bindless_layout(),
            pipeline,
            hdr_image: hdr.target_image.clone(),
            depth_image: primary.target_image.clone(),
        }
    }

    pub fn notify_resolution(&mut self, rmg: &mut Rmg, primary: &Primary, hdr: &Hdr) {
        let resolution = primary.target_image.extent_2d();
        if self.target_image.extent_2d() == resolution {
            return;
        }

        log::info!("Changing resolution to {:?}", resolution);

        let mut desc = self.target_image.image_desc().clone();
        desc.extent.width = resolution.width;
        desc.extent.height = resolution.height;
        self.target_image = rmg
            .new_image_uninitialized(desc, Some("hdr-ldr-target"))
            .unwrap();
        self.depth_image = primary.target_image.clone();
        self.hdr_image = hdr.target_image.clone();
    }

    pub fn dispatch_size(&self) -> [u32; 3] {
        [
            (self.target_image.extent_2d().width / 8) + 1,
            (self.target_image.extent_2d().height / 8) + 1,
            1,
        ]
    }
}

impl Task for HdrLdr {
    fn name(&self) -> &'static str {
        "Hdr To Ldr"
    }
    fn queue_flags(&self) -> vk::QueueFlags {
        vk::QueueFlags::GRAPHICS | vk::QueueFlags::COMPUTE
    }
    fn register(&self, registry: &mut marpii_rmg::ResourceRegistry) {
        registry
            .request_image(
                &self.target_image,
                vk::PipelineStageFlags2::COMPUTE_SHADER,
                vk::AccessFlags2::SHADER_STORAGE_WRITE,
                vk::ImageLayout::GENERAL,
            )
            .unwrap();
        registry
            .request_image(
                &self.hdr_image,
                vk::PipelineStageFlags2::COMPUTE_SHADER,
                vk::AccessFlags2::SHADER_STORAGE_WRITE,
                vk::ImageLayout::GENERAL,
            )
            .unwrap();
        registry
            .request_image(
                &self.depth_image,
                vk::PipelineStageFlags2::COMPUTE_SHADER,
                vk::AccessFlags2::SHADER_STORAGE_WRITE,
                vk::ImageLayout::GENERAL,
            )
            .unwrap();

        registry.register_asset(self.pipeline.clone());
    }

    fn pre_record(
        &mut self,
        resources: &mut marpii_rmg::Resources,
        ctx: &marpii_rmg::CtxRmg,
    ) -> Result<(), marpii_rmg::RecordError> {
        self.pc.get_content_mut().resolution = [
            self.target_image.extent_2d().width,
            self.target_image.extent_2d().height,
        ];
        self.pc.get_content_mut().ldr_image = resources
            .resource_handle_or_bind(self.target_image.clone())
            .unwrap();
        self.pc.get_content_mut().depth_image = resources
            .resource_handle_or_bind(self.depth_image.clone())
            .unwrap();
        self.pc.get_content_mut().hdr_image = resources
            .resource_handle_or_bind(self.hdr_image.clone())
            .unwrap();
        Ok(())
    }

    fn record(
        &mut self,
        device: &std::sync::Arc<marpii::context::Device>,
        command_buffer: &vk::CommandBuffer,
        _resources: &marpii_rmg::Resources,
    ) {
        unsafe {
            device.inner.cmd_bind_pipeline(
                *command_buffer,
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline.pipeline,
            );
            device.inner.cmd_push_constants(
                *command_buffer,
                self.pipeline.layout.layout,
                vk::ShaderStageFlags::ALL,
                0,
                self.pc.content_as_bytes(),
            );

            let [dx, dy, dz] = self.dispatch_size();
            device.inner.cmd_dispatch(*command_buffer, dx, dy, dz);
        }
    }
}
