use marpii::{
    ash::vk::{self, Extent2D},
    resources::{BufDesc, ComputePipeline, ImgDesc, PipelineLayout, PushConstant, ShaderStage},
    OoS,
};
use marpii_rmg::{BufferHandle, CtxRmg, ImageHandle, Rmg, SamplerHandle, Task};

use crate::{passes::primary::Primary, patcher::Patcher, Camera};
use shared::{glam::Vec3, spec_cache::SpecularCache};
use std::sync::Arc;

use super::auxdta::AuxData;

static SPECULAR_TRACE_SHADER: &'static [u8] =
    include_bytes!("../../../../assets/spirv/shader-specular-trace.spv");

pub struct SpecularTrace {
    patcher: Patcher,
    pipeline_layout: Arc<PipelineLayout>,
    pipeline: Arc<ComputePipeline>,
    last_resolution: Extent2D,
    pub specular_cache: BufferHandle<SpecularCache>,
    depth_image: ImageHandle,
    bluenoise_img: ImageHandle,
    bluenoise_sampler: SamplerHandle,
    pub pc: PushConstant<shared::SpecularTracePush>,
    is_last_reset: bool,
    epoch: u32,
}

impl SpecularTrace {
    pub fn new(rmg: &mut Rmg, primary: &Primary, aux: &AuxData) -> Self {
        let resolution = primary.target_image.extent_2d();
        let mut patcher = Patcher::new(
            rmg.ctx.device.clone(),
            SPECULAR_TRACE_SHADER.to_vec(),
            "specular_trace",
            &["eval_sdf", "eval_mro", "eval_albedo"],
        );
        let base_shader = patcher
            .fetch_new_module()
            .expect("Could not get base shader!");

        let mut pc = PushConstant::new(
            shared::SpecularTracePush::default(),
            vk::ShaderStageFlags::COMPUTE,
        );
        //Flag for reset
        pc.get_content_mut().reset_specular = 1;
        //overwrite bluenoise extent
        pc.get_content_mut().bluenoise_ext = [
            aux.bluenoise_extent.width,
            aux.bluenoise_extent.height,
            aux.bluenoise_extent.depth,
        ];

        let shader_stage = ShaderStage::from_module(
            base_shader,
            vk::ShaderStageFlags::COMPUTE,
            "specular_trace".to_owned(),
        );
        let pipeline = Arc::new(
            ComputePipeline::new(
                &rmg.ctx.device,
                &shader_stage,
                None,
                rmg.resources.bindless_layout(),
            )
            .unwrap(),
        );

        let cache_size = Self::cache_size(resolution);
        log::error!(
            "Allocating {}mb Specular-Cache",
            (cache_size * std::mem::size_of::<SpecularCache>()) / 1_000_000
        );

        let specular_cache = rmg
            .new_buffer_uninitialized(
                BufDesc::storage_buffer::<SpecularCache>(cache_size),
                Some("specular-cache"),
            )
            .unwrap();

        SpecularTrace {
            specular_cache,
            patcher,
            pc,
            last_resolution: resolution,
            pipeline_layout: rmg.resources.bindless_layout(),
            depth_image: primary.target_image.clone(),
            pipeline,
            is_last_reset: true,
            bluenoise_img: aux.bluenoise_rgba.clone(),
            bluenoise_sampler: aux.linear_sampler_clamp.clone(),
            epoch: 0,
        }
    }

    pub fn try_new_pipeline(&mut self, ctx: &CtxRmg) {
        let nm = if let Some(new_module) = self.patcher.fetch_new_module() {
            log::info!("New Shader module!");
            new_module
        } else {
            return;
        };

        let ss = ShaderStage::from_module(
            nm,
            vk::ShaderStageFlags::COMPUTE,
            "specular_trace".to_owned(),
        );

        match ComputePipeline::new(&ctx.device, &ss, None, self.pipeline_layout.clone()) {
            Ok(np) => {
                self.pipeline = Arc::new(np);
            }
            Err(e) => {
                log::error!("Failed to build new pipeline: {e}");
                return;
            }
        }
    }

    pub fn offset_parameter(&mut self, new: Vec3) {
        self.pc.get_content_mut().offset = new.into();
    }

    pub fn time(&mut self, time: f32) {
        self.pc.get_content_mut().time = time;
    }

    pub fn notify_resolution(&mut self, rmg: &mut Rmg, primary: &Primary) {
        let resolution = primary.target_image.extent_2d();
        if self.last_resolution == resolution {
            return;
        }

        log::info!("Changing resolution to {:?}", resolution);

        let cache_size = Self::cache_size(resolution);
        log::error!(
            "Allocating {}mb Specular-Cache",
            (cache_size * std::mem::size_of::<SpecularCache>()) / 1_000_000
        );

        let specular_cache = rmg
            .new_buffer_uninitialized(
                BufDesc::storage_buffer::<SpecularCache>(cache_size),
                Some("specular-cache"),
            )
            .unwrap();
        self.specular_cache = specular_cache;
        self.pc.get_content_mut().reset_specular = 1;
        self.is_last_reset = true;
        self.epoch = 0;
        self.depth_image = primary.target_image.clone();

        self.last_resolution = resolution;
    }

    fn cache_size(resolution: vk::Extent2D) -> usize {
        (((resolution.width / shared::gicache::CACHE_CLUSTER_SIZE) + 1)
            * shared::gicache::CACHE_CLUSTER_SIZE
            * ((resolution.height / shared::gicache::CACHE_CLUSTER_SIZE) + 1)
            * shared::gicache::CACHE_CLUSTER_SIZE) as usize
    }

    pub fn dispatch_size(&self) -> [u32; 3] {
        [
            (self.last_resolution.width / 8) + 1,
            (self.last_resolution.height / 8) + 1,
            1,
        ]
    }

    pub fn update_camera(&mut self, cam: &Camera) {
        let (origin, rotation, exposure) = cam.get_gpu_dta();
        self.pc.get_content_mut().camera.rotation = rotation;
        self.pc.get_content_mut().camera.pos = origin.into();
        self.pc.get_content_mut().camera.exposure = exposure;
    }
}

impl Task for SpecularTrace {
    fn name(&self) -> &'static str {
        "SpecularTrace"
    }
    fn queue_flags(&self) -> vk::QueueFlags {
        vk::QueueFlags::GRAPHICS | vk::QueueFlags::COMPUTE
    }
    fn register(&self, registry: &mut marpii_rmg::ResourceRegistry) {
        registry
            .request_buffer(
                &self.specular_cache,
                vk::PipelineStageFlags2::COMPUTE_SHADER,
                vk::AccessFlags2::SHADER_STORAGE_WRITE,
            )
            .unwrap();

        registry
            .request_image(
                &self.depth_image,
                vk::PipelineStageFlags2::COMPUTE_SHADER,
                vk::AccessFlags2::SHADER_STORAGE_READ,
                vk::ImageLayout::GENERAL,
            )
            .unwrap();
        registry
            .request_image(
                &self.bluenoise_img,
                vk::PipelineStageFlags2::COMPUTE_SHADER,
                vk::AccessFlags2::SHADER_SAMPLED_READ,
                vk::ImageLayout::GENERAL,
            )
            .unwrap();
        registry.request_sampler(&self.bluenoise_sampler).unwrap();
        registry.register_asset(self.pipeline.clone());
    }

    fn pre_record(
        &mut self,
        resources: &mut marpii_rmg::Resources,
        ctx: &marpii_rmg::CtxRmg,
    ) -> Result<(), marpii_rmg::RecordError> {
        self.try_new_pipeline(ctx);

        self.pc.get_content_mut().resolution =
            [self.last_resolution.width, self.last_resolution.height];
        self.pc.get_content_mut().specular_cache = resources
            .resource_handle_or_bind(self.specular_cache.clone())
            .unwrap();
        self.pc.get_content_mut().depth_image = resources
            .resource_handle_or_bind(self.depth_image.clone())
            .unwrap();

        if self.is_last_reset {
            self.pc.get_content_mut().reset_specular = 1;
            self.is_last_reset = false;
        } else {
            self.pc.get_content_mut().reset_specular = 0;
        }

        //Rebind, not really needed though
        self.pc.get_content_mut().bluenoise = resources
            .resource_handle_or_bind(self.bluenoise_img.clone())
            .expect("Failed to bind bluenoise");
        self.pc.get_content_mut().noise_sampler = resources
            .resource_handle_or_bind(self.bluenoise_sampler.clone())
            .expect("Failed to bind bluenoise sampler");

        self.pc.get_content_mut().epoch = self.epoch;
        self.epoch += 1;

        Ok(())
    }

    fn record(
        &mut self,
        device: &std::sync::Arc<marpii::context::Device>,
        command_buffer: &vk::CommandBuffer,
        _resources: &marpii_rmg::Resources,
    ) {
        unsafe {
            device.inner.cmd_bind_pipeline(
                *command_buffer,
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline.pipeline,
            );
            device.inner.cmd_push_constants(
                *command_buffer,
                self.pipeline.layout.layout,
                vk::ShaderStageFlags::ALL,
                0,
                self.pc.content_as_bytes(),
            );

            let [dx, dy, dz] = self.dispatch_size();
            device.inner.cmd_dispatch(*command_buffer, dx, dy, dz);
        }
    }
}
