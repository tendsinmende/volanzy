use marpii::ash::vk;

const BLUENOISE_DTA: &'static [u8] =
    include_bytes!("../../../../assets/textures/bluenoise_hdr_rgba.raw");

///Parses an bluenoise custom raw image as described here:
///https://momentsingraphics.de/3DBlueNoise.html#Conclusion
/// and programmed here:
/// https://github.com/MomentsInGraphics/BlueNoise/blob/master/BlueNoise.py#L265
pub fn blue_noise_raw_reader() -> (Vec<u8>, vk::Extent3D, vk::Format) {
    let mut dta_vec = BLUENOISE_DTA.to_vec();
    //read out the header to parse our data, then drop the header and return the data
    let (extent, format, drop_size) = {
        println!("Teddy ");
        let header: &[u32] = bytemuck::cast_slice(&dta_vec);
        assert!(
            header[0] == 1,
            "header version should be 1, was {}",
            header[0]
        );
        let n_channel = header[1];
        assert!(n_channel == 4, "n_channel must be 4, was {}", n_channel);
        let n_dims = header[2];

        let extent = match n_dims {
            3 => vk::Extent3D {
                height: header[3],
                width: header[3 + 1],
                depth: header[3 + 2],
            },
            _ => panic!("Can only load Dim==3, was {}", n_dims),
        };

        //per definition atm
        let format = vk::Format::R32G32B32A32_UINT;
        //Dropping static header u32 + each u32 describing an extent.
        (extent, format, (3 + n_dims) * 4)
    };

    log::error!("Loading {:?}, {:?}", extent, format);
    let img_data = dta_vec.split_off(drop_size as usize);
    assert!(img_data.len() % 4 == 0, "splitted wrong pixel wise"); // must be multiple of 4 byte per channel pixel
    assert!(img_data.len() % (4 * 4) == 0, "splitted wrong channel wise");

    let expected_length = 4 * 4 * extent.width * extent.height * extent.depth;
    assert!(
        expected_length as usize == img_data.len(),
        "expected {expected_length} was {}",
        img_data.len()
    );
    // println!("{img_data:?}");

    (img_data, extent, format)
}
