use marpii::{
    ash::vk::{self, Extent2D, ImageUsageFlags},
    resources::{
        ComputePipeline, ImgDesc, PipelineLayout, PushConstant, ShaderModule, ShaderStage,
    },
    OoS,
};
use marpii_rmg::{BufferHandle, CtxRmg, ImageHandle, Rmg, Task};

use crate::{
    passes::{gi_trace::GiTrace, primary::Primary},
    patcher::Patcher,
    Camera,
};
use shared::{gicache::GiCache, glam::Vec3};
use std::sync::Arc;

static IRR_SHADER: &'static [u8] = include_bytes!("../../../../assets/spirv/shader-irradiance.spv");
pub struct Irradiance {
    patcher: Patcher,
    pipeline_layout: Arc<PipelineLayout>,
    pipeline: Arc<ComputePipeline>,
    pub target_image: ImageHandle,
    depth_image: ImageHandle,
    gi_cache: BufferHandle<GiCache>,
    pub pc: PushConstant<shared::IrradiancePush>,
}

impl Irradiance {
    pub fn new(rmg: &mut Rmg, primary: &Primary, gitrace: &GiTrace) -> Self {
        let resolution = primary.target_image.extent_2d();
        let mut patcher = Patcher::new(
            rmg.ctx.device.clone(),
            IRR_SHADER.to_vec(),
            "irr",
            &["eval_sdf", "eval_mro", "eval_albedo"],
        );
        // let module = ShaderModule::new_from_bytes(&rmg.ctx.device, IRR_SHADER)
        // .expect("Could not load hdr-ldr shader");
        let base_shader = patcher
            .fetch_new_module()
            .expect("Could not get base shader!");

        let pc = PushConstant::new(
            shared::IrradiancePush::default(),
            vk::ShaderStageFlags::COMPUTE,
        );
        let shader_stage = ShaderStage::from_module(
            base_shader,
            vk::ShaderStageFlags::COMPUTE,
            "main".to_owned(),
        );
        let pipeline = Arc::new(
            ComputePipeline::new(
                &rmg.ctx.device,
                &shader_stage,
                None,
                rmg.resources.bindless_layout(),
            )
            .unwrap(),
        );

        let target_image = rmg
            .new_image_uninitialized(
                ImgDesc::storage_image_2d(
                    resolution.width,
                    resolution.height,
                    vk::Format::R32G32B32A32_SFLOAT,
                )
                .add_usage(ImageUsageFlags::COLOR_ATTACHMENT),
                Some("irradiance-target"),
            )
            .unwrap();

        Irradiance {
            target_image,
            patcher,
            pc,
            pipeline_layout: rmg.resources.bindless_layout(),
            depth_image: primary.target_image.clone(),
            gi_cache: gitrace.gi_cache.clone(),
            pipeline,
        }
    }

    pub fn try_new_pipeline(&mut self, ctx: &CtxRmg) {
        let nm = if let Some(new_module) = self.patcher.fetch_new_module() {
            log::info!("New Shader module!");
            new_module
        } else {
            return;
        };

        let ss = ShaderStage::from_module(nm, vk::ShaderStageFlags::COMPUTE, "main".to_owned());

        match ComputePipeline::new(&ctx.device, &ss, None, self.pipeline_layout.clone()) {
            Ok(np) => {
                self.pipeline = Arc::new(np);
            }
            Err(e) => {
                log::error!("Failed to build new pipeline: {e}");
                return;
            }
        }
    }

    pub fn offset_parameter(&mut self, new: Vec3) {
        self.pc.get_content_mut().offset = new.into();
    }

    pub fn time(&mut self, time: f32) {
        self.pc.get_content_mut().time = time;
    }

    pub fn notify_resolution(&mut self, rmg: &mut Rmg, primary: &Primary, gi_trace: &GiTrace) {
        let resolution = primary.target_image.extent_2d();
        if self.target_image.extent_2d() == resolution {
            return;
        }

        log::info!("Changing resolution to {:?}", resolution);

        let mut desc = self.target_image.image_desc().clone();
        desc.extent.width = resolution.width;
        desc.extent.height = resolution.height;
        self.target_image = rmg
            .new_image_uninitialized(desc, Some("irradiance-target"))
            .unwrap();
        self.gi_cache = gi_trace.gi_cache.clone();
        self.depth_image = primary.target_image.clone();
    }

    pub fn dispatch_size(&self) -> [u32; 3] {
        [
            (self.target_image.extent_2d().width / 8) + 1,
            (self.target_image.extent_2d().height / 8) + 1,
            1,
        ]
    }

    pub fn update_camera(&mut self, cam: &Camera) {
        let (origin, rotation, exposure) = cam.get_gpu_dta();
        self.pc.get_content_mut().camera.rotation = rotation;
        self.pc.get_content_mut().camera.pos = origin.into();
        self.pc.get_content_mut().camera.exposure = exposure;
    }
}

impl Task for Irradiance {
    fn name(&self) -> &'static str {
        "Irradiance"
    }
    fn queue_flags(&self) -> vk::QueueFlags {
        vk::QueueFlags::GRAPHICS | vk::QueueFlags::COMPUTE
    }
    fn register(&self, registry: &mut marpii_rmg::ResourceRegistry) {
        registry
            .request_image(
                &self.target_image,
                vk::PipelineStageFlags2::COMPUTE_SHADER,
                vk::AccessFlags2::SHADER_STORAGE_WRITE,
                vk::ImageLayout::GENERAL,
            )
            .unwrap();

        registry
            .request_image(
                &self.depth_image,
                vk::PipelineStageFlags2::COMPUTE_SHADER,
                vk::AccessFlags2::SHADER_STORAGE_READ,
                vk::ImageLayout::GENERAL,
            )
            .unwrap();
        registry
            .request_buffer(
                &self.gi_cache,
                vk::PipelineStageFlags2::COMPUTE_SHADER,
                vk::AccessFlags2::SHADER_STORAGE_READ,
            )
            .unwrap();
        registry.register_asset(self.pipeline.clone());
    }

    fn pre_record(
        &mut self,
        resources: &mut marpii_rmg::Resources,
        ctx: &marpii_rmg::CtxRmg,
    ) -> Result<(), marpii_rmg::RecordError> {
        self.try_new_pipeline(ctx);

        self.pc.get_content_mut().resolution = [
            self.target_image.extent_2d().width,
            self.target_image.extent_2d().height,
        ];
        self.pc.get_content_mut().target_image = resources
            .resource_handle_or_bind(self.target_image.clone())
            .unwrap();
        self.pc.get_content_mut().depth_image = resources
            .resource_handle_or_bind(self.depth_image.clone())
            .unwrap();
        self.pc.get_content_mut().gi_cache = resources
            .resource_handle_or_bind(self.gi_cache.clone())
            .unwrap();
        Ok(())
    }

    fn record(
        &mut self,
        device: &std::sync::Arc<marpii::context::Device>,
        command_buffer: &vk::CommandBuffer,
        _resources: &marpii_rmg::Resources,
    ) {
        unsafe {
            device.inner.cmd_bind_pipeline(
                *command_buffer,
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline.pipeline,
            );
            device.inner.cmd_push_constants(
                *command_buffer,
                self.pipeline.layout.layout,
                vk::ShaderStageFlags::ALL,
                0,
                self.pc.content_as_bytes(),
            );

            let [dx, dy, dz] = self.dispatch_size();
            device.inner.cmd_dispatch(*command_buffer, dx, dy, dz);
        }
    }
}
