use marpii_rmg_tasks::egui;

use crate::{camera::Camera, perf::Perf, settings::Settings};

#[derive(Clone)]
struct GuiState {
    draw_perf: bool,
    draw_camera: bool,
    draw_denoiser_settings: bool,
    draw_gi_settings: bool,
    draw_specular_settings: bool,
}

pub struct Gui {
    state: GuiState,
    pub perf: Perf,
}

impl Gui {
    pub fn new() -> Self {
        Gui {
            state: GuiState {
                draw_perf: false,
                draw_camera: false,
                draw_denoiser_settings: false,
                draw_gi_settings: false,
                draw_specular_settings: false,
            },
            perf: Perf::new(),
        }
    }
    pub fn run(
        &mut self,
        ctx: &marpii_rmg_tasks::egui::Context,
        camera: &mut Camera,
        settings: &mut Settings,
    ) {
        let mut state = self.state.clone();
        egui::TopBottomPanel::top("Tools").show(ctx, |egui| {
            egui.vertical(|ui| {
                egui::menu::bar(ui, |ui| {
                    ui.menu_button("File", |ui| {
                        if ui.button("close").clicked() {
                            println!("Yay!");
                        }
                    });

                    ui.menu_button("Tools", |ui| {
                        if ui.button("Performance").clicked() {
                            state.draw_perf = !state.draw_perf;
                            if state.draw_perf {
                                self.perf.sets.clear();
                            }
                        }
                        if ui.button("Camera Settings").clicked() {
                            state.draw_camera = !state.draw_camera;
                        }

                        if ui.button("Denoiser Settings").clicked() {
                            state.draw_denoiser_settings = !state.draw_denoiser_settings;
                        }
                        if ui.button("Gi Settings").clicked() {
                            state.draw_gi_settings = !state.draw_gi_settings;
                        }
                        if ui.button("Specular Settings").clicked() {
                            state.draw_specular_settings = !state.draw_specular_settings;
                        }
                    });
                })
            })
        });

        if state.draw_perf {
            egui::Window::new("Frametime")
                .open(&mut state.draw_perf)
                .show(ctx, |ui| {
                    if ui.button("Clear Perf").clicked() {
                        self.perf.sets.clear();
                    }
                    ui.add(egui_widgets::TimelinePlot::new(
                        self.perf.into_plot().as_slice(),
                    ));
                });
        }

        if state.draw_camera {
            egui::Window::new("Camera Settings")
                .open(&mut state.draw_camera)
                .show(ctx, |ui| {
                    if ui.button("Clear Perf").clicked() {
                        self.perf.sets.clear();
                    }
                    ui.horizontal(|ui| {
                        ui.label("Aperture");
                        ui.add(egui::Slider::new(&mut camera.aperture, (1.0 / 64.0)..=1.0))
                    });
                    ui.horizontal(|ui| {
                        ui.label("Shutter Speed");
                        ui.add(egui::Slider::new(
                            &mut camera.shutter_speed,
                            (1.0 / 1000.0)..=1.0,
                        ))
                    });
                    ui.horizontal(|ui| {
                        ui.label("ISO");
                        ui.add(egui::Slider::new(&mut camera.sensitivity, 100.0..=6000.0))
                    });
                });
        }

        if state.draw_denoiser_settings {
            egui::Window::new("Denoiser Settings")
                .open(&mut state.draw_denoiser_settings)
                .show(ctx, |ui| {
                    ui.horizontal(|ui| {
                        ui.label("ωp: normal reject");
                        ui.add(egui::Slider::new(
                            &mut settings.denoiser.gi_sigma_normal,
                            0.0..=256.0,
                        )).on_hover_ui(|ui|{ui.label("How much different the sample's normal can be. This is a weighted dot product between a samples, and the kernels normal. Usually ~ 128.0");});
                    });
                    ui.horizontal(|ui| {
                        ui.label("ωp: Max positional divergence");
                        ui.add(egui::Slider::new(
                            &mut settings.denoiser.gi_sigma_position,
                            0.01..=10.0,
                        )).on_hover_ui(|ui|{ui.label("How far apart in worldspace samples in the denoiser can be. Typically in the 0.1..10.0 range. But really depends on your type of content");});
                    });
                    ui.horizontal(|ui| {
                        ui.label("ωl: Luminace divergence");
                        ui.add(egui::Slider::new(
                            &mut settings.denoiser.gi_sigma_luminance,
                            0.0..=10.0,
                        )).on_hover_ui(|ui| {ui.label("Howmuch the luminance divergence is weighted.");});
                    });
                });
        }

        if state.draw_gi_settings {
            egui::Window::new("GI Settings")
                .open(&mut state.draw_gi_settings)
                .show(ctx, |ui| {
                    ui.horizontal(|ui| {
                        ui.label("GI Bounces");
                        ui.add(egui::Slider::new(&mut settings.gi_trace.bounces, 0..=10))
                            .on_hover_ui(|ui| {
                                ui.label("How often a GI ray will (max) bounce. 0 disables GI");
                            });
                    });
                    ui.horizontal(|ui| {
                        ui.label("GI Max tracing distance");
                        ui.add(egui::Slider::new(
                            &mut settings.gi_trace.gi_max_trace,
                            0.1..=1000.0,
                        ))
                        .on_hover_ui(|ui| {
                            ui.label("How far a GI ray can travel till its considered dead");
                        });
                    });
                    ui.horizontal(|ui| {
                        ui.label("History percentage");
                        ui.add(egui::Slider::new(
                            &mut settings.gi_trace.gi_carry_over_weight,
                            0.0..=1.0,
                        ))
                        .on_hover_ui(|ui| {
                            ui.label(
                                "How much of the GI's history is considered for the next frame",
                            );
                        });
                    });
                    ui.horizontal(|ui| {
                        ui.label("Depth reject multiplier");
                        ui.add(egui::Slider::new(
                            &mut settings.gi_trace.trace_cache_depth_reject_mul,
                            0.1..=10.0,
                        ))
                        .on_hover_ui(|ui| {
                            ui.label(
                                "Scales the depth-based gi-cache rejection. Lower values will trash the cache faster when moving.",
                            );
                        });
                    });
                    ui.horizontal(|ui| {
                        ui.label("Normal reject multiplier");
                        ui.add(egui::Slider::new(
                            &mut settings.gi_trace.trace_cache_normal_reject,
                            0.0..=1.0,
                        ))
                        .on_hover_ui(|ui| {
                            ui.label(
                                "Scales the normal-based gi-cache rejection. Higher values will trash the cache faster if the normals on a given pixel change",
                            );
                        });
                    });
                    ui.horizontal(|ui| {
                        ui.label("Age increase");
                        ui.add(egui::Slider::new(
                            &mut settings.gi_trace.trace_cache_age_add,
                            0.0..=1.0,
                        ))
                        .on_hover_ui(|ui| {
                            ui.label(
                                "Scales how much the age of a cache-cell increases with each successful accumulation"
                            );
                        });
                    });
                    ui.horizontal(|ui| {
                        ui.label("Max age");
                        ui.add(egui::Slider::new(
                            &mut settings.gi_trace.trace_cache_age_max,
                            0.0..=10.0,
                        ))
                        .on_hover_ui(|ui| {
                            ui.label(
                                "The age cap of the gi-cells. Higher cap means that younger cells are considered less in the denoising pass."
                            );
                        });
                    });
                });
        }
        if state.draw_specular_settings {
            egui::Window::new("Specular Settings")
                .open(&mut state.draw_specular_settings)
                .show(ctx, |ui| {
                    ui.horizontal(|ui| {
                        ui.label("Ray bounces");
                        ui.add(egui::Slider::new(
                            &mut settings.specular_trace.specular_bounces,
                            0..=10,
                        ));
                    });
                    ui.horizontal(|ui| {
                        ui.label("Max trace distance");
                        ui.add(egui::Slider::new(
                            &mut settings.specular_trace.specular_max_trace,
                            0.0..=100.0,
                        ));
                    });
                    ui.horizontal(|ui| {
                        ui.label("CarryOver Epsilon");
                        ui.add(egui::Slider::new(
                            &mut settings.specular_trace.specular_carry_over_epsilon,
                            0.0..=1.0,
                        ));
                    });
                    ui.horizontal(|ui| {
                        ui.label("Roughness reject");
                        ui.add(egui::Slider::new(
                            &mut settings.specular_trace.specular_roughness_reject,
                            0.0..=1.0,
                        ));
                    });
                    ui.horizontal(|ui| {
                        ui.label("Positional Reject radius");
                        ui.add(egui::Slider::new(
                            &mut settings.specular_trace.trace_position_reject,
                            0.0..=10.0,
                        ));
                    });
                    ui.horizontal(|ui| {
                        ui.label("Normal reject cosine");
                        ui.add(egui::Slider::new(
                            &mut settings.specular_trace.trace_normal_reject,
                            0.0..=1.0,
                        ));
                    });
                });
        }

        self.state = state;
    }
}
