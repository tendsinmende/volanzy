//! A really simple sphere trace renderer.
//!
//! uses the `sdf.vola` file to runtime-patch the sphere-tracing shader with new code.
//!
//! This is a test intersection of two projects. The [vola](https://www.gitlab.com/tendsinmende/vola) compiler, and the [spv-patcher](https://www.gitlab.com/tendsinmende/spv-patcher).

use std::time::Instant;

use camera::Camera;
use marpii::{ash::vk::Extent2D, context::Ctx};
use marpii_rmg::{Rmg, Task};
use marpii_rmg_tasks::SwapchainPresent;
use offset_entity::OffsetEntity;
use shared::glam::{EulerRot, Quat, Vec3};
use winit::{
    event::{DeviceEvent, ElementState, Event, KeyEvent, MouseButton, WindowEvent},
    event_loop::ControlFlow,
    keyboard::{Key, NamedKey, PhysicalKey},
};

mod camera;
pub mod gui;
mod offset_entity;
mod passes;
mod patcher;
mod perf;
mod settings;

fn main() {
    simple_logger::SimpleLogger::new()
        .with_level(log::LevelFilter::Error)
        .init()
        .unwrap();

    let ev = winit::event_loop::EventLoop::new().unwrap();

    let windowattr = winit::window::Window::default_attributes().with_title("Volanzy");
    #[allow(deprecated)]
    let window = ev.create_window(windowattr).unwrap();
    let (context, surface) = Ctx::default_with_surface(&window, true).unwrap();
    let mut rmg = Rmg::new(context).unwrap();

    let mut settings = settings::Settings::default();
    let mut gui = gui::Gui::new();
    let mut renderer = passes::Renderer::new(&mut rmg, surface, &ev, &settings);
    let mut camera = Camera::default();
    let mut offset_entity = OffsetEntity::new();

    let start = Instant::now();

    let mut cgrab = winit::window::CursorGrabMode::None;
    #[allow(deprecated)]
    let _ = ev.run(move |ev, ev_loop| {
        ev_loop.set_control_flow(winit::event_loop::ControlFlow::Poll);

        if cgrab != winit::window::CursorGrabMode::None {
            camera.on_event(&ev);
        }
        offset_entity.on_event(&ev);

        renderer.egui.handle_event(&window, &ev);

        match ev {
            Event::LoopExiting => {
                rmg.wait_for_idle().unwrap();
            }
            Event::AboutToWait => window.request_redraw(),
            Event::WindowEvent {
                window_id: _,
                event: WindowEvent::RedrawRequested,
            } => {
                camera.update();
                offset_entity.update();

                renderer.update_camera(&camera);
                renderer.update_offset_param(offset_entity.offset_parameter);
                renderer.update_time(start.elapsed().as_secs_f32());
                let resolution = window.inner_size();
                renderer.notify_resolution(
                    &mut rmg,
                    Extent2D {
                        width: resolution.width,
                        height: resolution.height,
                    },
                );

                //run egui integration
                renderer
                    .egui
                    .run(&mut rmg, &window, |ctx| {
                        gui.run(ctx, &mut camera, &mut settings);
                    })
                    .expect("Failed to run gui");

                renderer.update_settings(&settings);

                renderer.tick_perf(&mut rmg, &mut gui.perf);
                renderer.redraw(&mut rmg);
            }
            /*
            Event::WindowEvent {
                window_id: _,
                event:
                    winit::event::WindowEvent::KeyboardInput {
                        device_id: _,
                        event:
                            KeyEvent {
                                logical_key: Key::Named(NamedKey::Escape),
                                state: ElementState::Released,
                                ..
                            },
                        is_synthetic: _,
                    },
            } => *cf = ev_loop.exit(),
            */
            Event::WindowEvent {
                window_id: _,
                event:
                    WindowEvent::MouseInput {
                        state,
                        button: MouseButton::Right,
                        ..
                    },
            } => {
                let next = if state == ElementState::Pressed {
                    winit::window::CursorGrabMode::Locked
                } else {
                    winit::window::CursorGrabMode::None
                };

                if let Err(e) = window.set_cursor_grab(next) {
                    log::error!("Failed to grab cursor: {e}");
                }

                cgrab = next;
            }
            _ => {}
        }
    });
}
