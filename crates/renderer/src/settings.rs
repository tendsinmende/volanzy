#[derive(Clone, Debug)]
pub struct DenoiserSettings {
    pub gi_sigma_normal: f32,
    pub gi_sigma_position: f32,
    pub gi_sigma_luminance: f32,

    pub specular_sigma_normal: f32,
    pub specular_sigma_position: f32,
    pub specular_sigma_luminance: f32,
}

impl Default for DenoiserSettings {
    fn default() -> Self {
        DenoiserSettings {
            gi_sigma_normal: 128.0,
            gi_sigma_position: 1.0,
            gi_sigma_luminance: 4.0,

            specular_sigma_normal: 128.0,
            specular_sigma_position: 1.0,
            specular_sigma_luminance: 4.0,
        }
    }
}

#[derive(Clone, Debug)]
pub struct GiSettings {
    ///scaling from frame resolution to screen-space buffer resolution. Should typically be 0.5..1.0
    pub buffer_scaling: f32,
    ///Number of GI-Trace bounces per frame
    pub bounces: u32,
    ///How far a gi ray can travel
    pub gi_max_trace: f32,
    pub gi_carry_over_weight: f32,
    pub trace_cache_depth_reject_mul: f32,
    pub trace_cache_normal_reject: f32,
    pub trace_cache_age_add: f32,
    pub trace_cache_age_max: f32,
}

impl Default for GiSettings {
    fn default() -> Self {
        GiSettings {
            buffer_scaling: 1.0,
            bounces: 2,
            gi_max_trace: 25.0,
            gi_carry_over_weight: 0.95,
            trace_cache_depth_reject_mul: 1.0,
            trace_cache_age_add: 1.0,
            trace_cache_age_max: 2.0,
            trace_cache_normal_reject: 0.5,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SpecularSettings {
    pub specular_bounces: u32,
    pub specular_max_trace: f32,
    pub specular_carry_over_epsilon: f32,
    pub specular_roughness_reject: f32,
    pub trace_position_reject: f32,
    pub trace_normal_reject: f32,
}

impl Default for SpecularSettings {
    fn default() -> Self {
        SpecularSettings {
            specular_bounces: 3,
            specular_max_trace: 100.0,
            specular_carry_over_epsilon: 0.95,
            specular_roughness_reject: 0.1,
            trace_position_reject: 1.0,
            trace_normal_reject: 0.3,
        }
    }
}

#[derive(Default, Debug, Clone)]
pub struct Settings {
    pub denoiser: DenoiserSettings,
    pub gi_trace: GiSettings,
    pub specular_trace: SpecularSettings,
}
