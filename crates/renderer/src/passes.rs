mod auxdta;
mod bluenoise;
mod gi_trace;
mod hdr;
mod hdrldr;
mod irradiance;
mod primary;
mod specular;
mod specular_trace;

use crate::{
    perf::Perf,
    settings::{self, Settings},
    Camera,
};
use auxdta::AuxData;
use marpii::{ash::vk::Extent2D, surface::Surface, OoS};
use marpii_rmg::Rmg;
use marpii_rmg_tasks::SwapchainPresent;
use shared::glam::Vec3;

pub struct Renderer {
    pub primary: primary::Primary,
    pub specular: specular::Specular,
    pub gitrace: gi_trace::GiTrace,
    pub speculartrace: specular_trace::SpecularTrace,
    pub irradiance: irradiance::Irradiance,
    pub hdr: hdr::Hdr,
    pub hdrldr: hdrldr::HdrLdr,
    pub present: SwapchainPresent,
    pub aux: AuxData,
    pub egui: marpii_rmg_tasks::EGuiWinitIntegration,
    pub frame_index: usize,
}

impl Renderer {
    pub fn new(
        rmg: &mut Rmg,
        surface: OoS<Surface>,
        eventloop: &winit::event_loop::EventLoop<()>,
        settings: &Settings,
    ) -> Self {
        let aux = AuxData::new(rmg);
        let present = SwapchainPresent::new(rmg, surface).unwrap();
        let primary = primary::Primary::new(
            rmg,
            present.extent().unwrap_or(Extent2D {
                width: 1,
                height: 1,
            }),
        );
        let gitrace = gi_trace::GiTrace::new(rmg, &primary, &aux);
        let speculartrace = specular_trace::SpecularTrace::new(rmg, &primary, &aux);
        let specular = specular::Specular::new(rmg, &primary, &speculartrace);
        let irradiance = irradiance::Irradiance::new(rmg, &primary, &gitrace);
        let hdr = hdr::Hdr::new(rmg, &specular, &irradiance);
        let hdrldr = hdrldr::HdrLdr::new(rmg, &primary, &hdr);
        let egui = marpii_rmg_tasks::EGuiWinitIntegration::new(rmg, eventloop)
            .expect("Failed to create egui");

        let mut renderer = Renderer {
            primary,
            specular,
            gitrace,
            speculartrace,
            irradiance,
            hdr,
            hdrldr,
            present,
            aux,
            egui,
            frame_index: 0,
        };

        renderer.update_settings(&settings);
        renderer
    }

    pub fn tick_perf(&mut self, rmg: &mut Rmg, perf: &mut Perf) {
        let new_stamps = rmg.get_recent_track_timings_blocking();
        for stamp in new_stamps {
            if stamp.name == "DynamicBuffer" || stamp.name == "DynamicImage" {
                continue;
            }
            if let Some(perf_stamps) = perf.sets.get_mut(&stamp.name) {
                perf_stamps.push((self.frame_index as u32, stamp.timing));
            } else {
                let mut buf = fixring::RingBuffer::new();
                buf.push((self.frame_index as u32, stamp.timing));
                perf.sets.insert(stamp.name, buf);
            }
        }
    }

    pub fn update_camera(&mut self, camera: &Camera) {
        self.primary.update_camera(camera);
        self.specular.update_camera(camera);
        self.gitrace.update_camera(camera);
        self.speculartrace.update_camera(camera);
        self.irradiance.update_camera(camera);
        self.hdr.update_camera(camera);
    }

    pub fn update_offset_param(&mut self, offset: Vec3) {
        self.primary.offset_parameter(offset);
        self.specular.offset_parameter(offset);
        self.gitrace.offset_parameter(offset);
        self.speculartrace.offset_parameter(offset);
        self.irradiance.offset_parameter(offset);
        self.hdr.offset_parameter(offset);
    }

    pub fn update_time(&mut self, elapsed: f32) {
        self.primary.time(elapsed);
        self.specular.time(elapsed);
        self.gitrace.time(elapsed);
        self.speculartrace.time(elapsed);
        self.irradiance.time(elapsed);
        self.hdr.time(elapsed);
    }

    pub fn notify_resolution(&mut self, rmg: &mut Rmg, res: Extent2D) {
        self.primary.notify_resolution(rmg, res);

        self.gitrace.notify_resolution(rmg, &self.primary);
        self.speculartrace.notify_resolution(rmg, &self.primary);

        self.specular
            .notify_resolution(rmg, &self.primary, &self.speculartrace);
        self.irradiance
            .notify_resolution(rmg, &self.primary, &self.gitrace);
        self.hdr
            .notify_resolution(rmg, &self.specular, &self.irradiance);
        self.hdrldr.notify_resolution(rmg, &self.primary, &self.hdr);
        self.egui
            .renderer_mut()
            .set_resolution(rmg, res)
            .expect("Failed to update egui resolution");
    }

    pub fn update_settings(&mut self, settings: &Settings) {
        //update gi denoiser
        {
            let pc = self.irradiance.pc.get_content_mut();
            pc.gi_denoise_sigma_normal = settings.denoiser.gi_sigma_normal;
            pc.gi_deniose_sigma_pos = settings.denoiser.gi_sigma_position;
            pc.gi_denoise_sigma_luminance = settings.denoiser.gi_sigma_luminance;
        }
        //update specular denoiser
        {
            let pc = self.specular.pc.get_content_mut();
            pc.spec_denoise_sigma_normal = settings.denoiser.specular_sigma_normal;
            pc.spec_deniose_sigma_pos = settings.denoiser.specular_sigma_position;
            pc.spec_denoise_sigma_luminance = settings.denoiser.specular_sigma_luminance;
        }

        //update gi settings
        {
            let pc = self.gitrace.pc.get_content_mut();
            pc.bounces = settings.gi_trace.bounces;
            pc.gi_max_trace = settings.gi_trace.gi_max_trace;
            pc.gi_carry_over_weight = settings.gi_trace.gi_carry_over_weight;
            pc.trace_cache_depth_reject_mul = settings.gi_trace.trace_cache_depth_reject_mul;
            pc.trace_cache_normal_reject = settings.gi_trace.trace_cache_normal_reject;
            pc.trace_cache_age_add = settings.gi_trace.trace_cache_age_add;
            pc.trace_cache_age_max = settings.gi_trace.trace_cache_age_max;
        }

        //update specular trace settings
        {
            let pc = self.speculartrace.pc.get_content_mut();
            pc.specular_carry_epsilon = settings.specular_trace.specular_carry_over_epsilon;
            pc.specular_max_trace = settings.specular_trace.specular_max_trace;
            pc.specular_roughness_recject = settings.specular_trace.specular_roughness_reject;
            pc.trace_cache_normal_reject = settings.specular_trace.trace_normal_reject;
            pc.trace_cache_postion_reject = settings.specular_trace.trace_position_reject;
            pc.bounces = settings.specular_trace.specular_bounces;
        }
    }

    pub fn redraw(&mut self, rmg: &mut Rmg) {
        //Build the pass

        self.egui
            .renderer_mut()
            .set_source_image(self.hdrldr.target_image.clone());

        self.present.push_image(
            self.egui.target_image().clone(),
            self.egui.target_image().extent_2d(),
        );

        /*
        self.present.push_image(
            self.specular.target_image.clone(),
            self.specular.target_image.extent_2d(),
        );
        */

        let recorder = rmg
            .record()
            .add_task(&mut self.primary)
            .unwrap()
            .add_task(&mut self.speculartrace)
            .unwrap()
            .add_task(&mut self.gitrace)
            .unwrap()
            .add_task(&mut self.irradiance)
            .unwrap()
            .add_task(&mut self.specular)
            .unwrap()
            .add_task(&mut self.hdr)
            .unwrap()
            .add_task(&mut self.hdrldr)
            .unwrap()
            .add_meta_task(self.egui.renderer_mut())
            .unwrap();

        let recorder = match recorder.add_task(&mut self.present) {
            Err(e) => {
                log::error!("Failed to present: {e}");
                return;
            }
            Ok(r) => r,
        };

        recorder.execute().unwrap();
        self.frame_index += 1;
    }
}
