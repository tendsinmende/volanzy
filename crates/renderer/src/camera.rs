use std::time::Instant;

use shared::glam::{EulerRot, Quat, Vec3};
use winit::{
    event::{DeviceEvent, ElementState, Event, KeyEvent, WindowEvent},
    keyboard::{Key, KeyCode, NamedKey, PhysicalKey, SmolStr},
};

#[derive(Clone, Copy)]
struct MoveState {
    neg: bool,
    pos: bool,
}

impl Default for MoveState {
    fn default() -> Self {
        MoveState {
            neg: false,
            pos: false,
        }
    }
}

impl MoveState {
    fn into_velocity(&self) -> f32 {
        match (self.neg, self.pos) {
            (true, true) => 0.0,
            (false, true) => 1.0,
            (true, false) => -1.0,
            (false, false) => 0.0,
        }
    }
}

pub struct Camera {
    location: Vec3,
    rotation: Quat,
    last_update: Instant,

    move_states: [MoveState; 3],
    speedup: bool,
    ///Camera's aperture, usually something between 1/1.4 to 1/16.
    pub aperture: f32,
    /// shutter speed in seconds
    pub shutter_speed: f32,
    ///sensitivity, expressed as ISO
    pub sensitivity: f32,
}

impl Default for Camera {
    fn default() -> Self {
        Camera {
            location: Vec3::new(0.0, -2.0, -5.0),
            rotation: Quat::from_euler(EulerRot::XYZ, 0.0, -45.0f32.to_radians(), 0.0),
            last_update: Instant::now(),
            speedup: false,
            move_states: [MoveState::default(); 3],
            aperture: 1.0 / 8.0,
            shutter_speed: 1.0 / 100.0,
            sensitivity: 100.0,
        }
    }
}

impl Camera {
    const CAM_SPEEDUP: f32 = 0.001;

    const PERP_NEG: KeyCode = KeyCode::KeyA;
    const PERP_POS: KeyCode = KeyCode::KeyD;

    const HOR_NEG: KeyCode = KeyCode::KeyW;
    const HOR_POS: KeyCode = KeyCode::KeyS;

    const VERT_NEG: KeyCode = KeyCode::KeyE;
    const VERT_POS: KeyCode = KeyCode::KeyQ;

    pub fn on_event(&mut self, event: &winit::event::Event<()>) {
        match event {
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        event:
                            KeyEvent {
                                logical_key,
                                physical_key,
                                state,
                                ..
                            },
                        ..
                    },
                ..
            } => {
                match *physical_key{
                    PhysicalKey::Code(Self::PERP_NEG) => {
                        self.move_states[0].neg = *state == ElementState::Pressed
                    }
                    PhysicalKey::Code(Self::PERP_POS) => {
                        self.move_states[0].pos = *state == ElementState::Pressed
                    }

                    PhysicalKey::Code(Self::HOR_NEG) => {
                        self.move_states[2].neg = *state == ElementState::Pressed
                    }
                    PhysicalKey::Code(Self::HOR_POS) => {
                        self.move_states[2].pos = *state == ElementState::Pressed
                    }

                    PhysicalKey::Code(Self::VERT_NEG) => {
                        self.move_states[1].neg = *state == ElementState::Pressed
                    }
                    PhysicalKey::Code(Self::VERT_POS) => {
                        self.move_states[1].pos = *state == ElementState::Pressed
                    }
                    _ => {}
                }

                match logical_key{
                    Key::Named(NamedKey::ArrowLeft) => {
                        self.move_states[0].neg = *state == ElementState::Pressed
                    }
                    Key::Named(NamedKey::ArrowRight) => {
                        self.move_states[0].pos = *state == ElementState::Pressed
                    }

                    Key::Named(NamedKey::ArrowDown) => {
                        self.move_states[2].neg = *state == ElementState::Pressed
                    }
                    Key::Named(NamedKey::ArrowUp) => {
                        self.move_states[2].pos = *state == ElementState::Pressed
                    }
                    Key::Named(NamedKey::Shift) => self.speedup = *state == ElementState::Pressed,
                    _ => {}
                }

            }
            Event::DeviceEvent {
                event: DeviceEvent::MouseMotion { delta },
                ..
            } => {
                let right = self.rotation.mul_vec3(Vec3::new(1.0, 0.0, 0.0));
                let rot_yaw = Quat::from_rotation_y(delta.0 as f32 * Self::CAM_SPEEDUP);
                let rot_pitch = Quat::from_axis_angle(right, delta.1 as f32 * Self::CAM_SPEEDUP);

                let to_add = rot_yaw * rot_pitch;
                self.rotation = to_add * self.rotation;
            }
            _ => {}
        }
    }

    pub fn update(&mut self) {
        let delta = self.last_update.elapsed().as_secs_f32();
        self.last_update = Instant::now();

        let mut velocity = Vec3::new(
            self.move_states[0].into_velocity(),
            self.move_states[1].into_velocity(),
            self.move_states[2].into_velocity(),
        );

        if self.speedup {
            velocity *= 10.0;
        }
        let velo_div = self.rotation.mul_vec3(velocity);

        self.location += velo_div * delta;
    }

    ///Computs the exposure setting for ISO100
    pub fn exposure(&self, ev100: f32) -> f32 {
        1.0 / (2.0f32.powf(ev100) * 1.2)
    }
    ///Returns the exposure value calculated from the camera exposure settings
    pub fn ev100(&self) -> f32 {
        ((self.aperture * self.aperture) / self.shutter_speed * 100.0 / self.sensitivity).log2()
    }

    pub fn get_gpu_dta(&self) -> ([f32; 3], [f32; 4], f32) {
        //println!("{} @ {}", self.location, self.rotation.mul_vec3(Vec3::Z));
        (
            self.location.into(),
            self.rotation.to_array(),
            self.exposure(self.ev100()),
        )
    }
}
