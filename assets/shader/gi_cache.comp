#version 460

#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_nonuniform_qualifier : require


#include "inc/camera.glsl"
//NOTE includes rmg inplicitly
#include "inc/gicache.glsl"
#include "inc/storage_image.glsl"




layout( push_constant ) uniform GiPush{
    Camera camera;
    uvec2 resolution;
    ResHandle gicache;
    ResHandle depth_image;

    vec3 p_offset;
    float p_time;

    //Half extent of the gi bound
    vec3 gi_halfext;
    float pad0;
    vec3 gi_pos;
    float pad1;
} push;


GiDesc gidesc(){
    return GiDesc(
        push.gicache,
        0,
        uvec2(0),

        push.gi_pos,
        0.0,
        push.gi_halfext,
        0.0
    );
}

layout(set = 0, binding = 0) buffer GiCache{
    GiCacheCell cells[];
} gibuffer[];



float eval_sdf( in vec3 pos, in vec3 offset, in float time){
    return length(pos + offset) - sin(time);
}

vec3 eval_mro(vec3 pos, vec3 offset, float time){
    return vec3(length(pos + offset) - sin(time));
}

vec3 eval_albedo(vec3 pos, vec3 offset, float time){
    return vec3(length(pos + offset) - cos(time));
}


//NOTE: The GICache pass works _in screen space_. We basically only update cells that are seen 
//      Atm.
layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
void main(){
    uvec2 wid = gl_GlobalInvocationID.xy;
    ivec2 coord = ivec2(wid);
    //Bail for out-of_buffer traces
    if (coord.x >= push.resolution.x || coord.y >= push.resolution.y){
      return;
    }

    vec2 coord_uv = vec2(coord) / vec2(push.resolution.x, push.resolution.y);
    vec2 ndc = coord_uv * 2.0 - 1.0;
    Ray ray = ray_from_ndc(push.camera, ndc, push.resolution);

    //Bail on invalid handle as well.
    if (handle_invalid(push.gicache) || handle_invalid(push.depth_image)){
        return;
    }

    //reconstruct ray position from depth, and shade the voxel accordingly
    float depth = imageLoad(
        storage_image_2d_r32f[nonuniformEXT(get_index(push.depth_image))],
        coord
    ).r;


    vec3 wspos = ray_at(ray, depth);
    
    //load the gi descriptor, get the index, and store the ray pos (atm)
    GiDesc gid = gidesc();
    bool is_invalid = false;
    uint idx = hash_pos(gid, wspos, is_invalid);

    if (is_invalid){
        return;
    }


    vec3 pos = wspos;
    vec3 offset = push.p_offset;
    float t = push.p_time;
    float sdf = eval_sdf(pos, offset, t);
    vec3 mro = eval_mro(pos, offset, t);
    vec3 albedo = eval_albedo(pos, offset, t);

    //is valid, therfore store the pos there
    gibuffer[get_index(push.gicache)].cells[idx].radiance = 
    vec3(max(sdf, 1.0))
    * max(mro, vec3(1.0)) 
    * clamp(albedo, vec3(1.0), vec3(1.0)) 
    * (wspos / 10.0);
}



