#include "ray.glsl"
#include "quat.glsl"

#define PI 3.14159265 

struct Camera{
    vec3 pos;
    float fov;
    //TODO: can we type-def to quad already?
    vec4 rotation;
};


float aspect_ratio(uvec2 resolution){
  return float(resolution.x) / float(resolution.y);  
}

Ray ray_from_ndc(in Camera camera, vec2 ndc, uvec2 resolution){
    float ar = aspect_ratio(resolution);
    float px = ndc.x * tan(camera.fov / 2.0 * PI / 180.0) * ar;
    float py = ndc.y * tan(camera.fov / 2.0 * PI / 180.0);

    vec3 dir = vec3(px, py, 1.0);
    
    vec3 wsdir = quat_mul_vec3(quat_from_vec4(camera.rotation), dir);

    return Ray(camera.pos, 300.0, wsdir, 0.0);
}
