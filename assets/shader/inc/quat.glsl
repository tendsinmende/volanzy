

///Quaternion rotation
struct Quat{
    vec3 xyz;
    float q;
};

Quat quat_from_vec4(vec4 xyzw){
    return Quat(xyzw.xyz, xyzw.w);
}

//Pulled from here: https://www.geeks3d.com/20141201/how-to-rotate-a-vertex-by-a-quaternion-in-glsl/
vec3 quat_mul_vec3(in Quat quat, vec3 vec){
    return vec + 2.0 * cross(quat.xyz, cross(quat.xyz, vec) + quat.q * vec);
}

