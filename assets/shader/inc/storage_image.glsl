
//NOTE: 1-channel common formats
layout(set = 1, binding = 0, r8) uniform image2D storage_image_2d_r8[];
layout(set = 1, binding = 0, r16f) uniform image2D storage_image_2d_r16f[];
layout(set = 1, binding = 0, r32f) uniform image2D storage_image_2d_r32f[];

//NOTE: 4-channel common formats
layout(set = 1, binding = 0, rgba8) uniform image2D storage_image_2d_rgba8[];
layout(set = 1, binding = 0, rgba16f) uniform image2D storage_image_2d_rgba16f[];
layout(set = 1, binding = 0, rgba32f) uniform image2D storage_image_2d_rgba32f[];


