

#define TYPE_STORAGE_BUFFER 0x00
#define TYPE_STORAGE_IMAGE 0x01
#define TYPE_SAMPLED_IMAGE 0x02
#define TYPE_SAMPLE 0x03
#define TYPE_ACCELERATION_STRUCTURE 0x04
#define TYPE_INVALID 0xff


struct ResHandle{
    uint hdl;
};

uint get_index(ResHandle res){
    return (res.hdl >> 8);
}
uint get_type(ResHandle res){
    return res.hdl & 0xff;
}

bool handle_invalid(ResHandle res){
  //checks that the handle is somewhere in the *undefined* area
  return get_type(res) > TYPE_ACCELERATION_STRUCTURE;
}

bool handle_valid(ResHandle res){
	return !handle_invalid(res);
}
