struct Ray{
    vec3 origin;
    float maxt;
    vec3 direction;
    float pad0;
};


vec3 ray_at(in Ray ray, float at){
    return ray.origin + ray.direction * at;
}
