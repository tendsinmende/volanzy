#include "rmg.glsl"

#define GI_CACHE_SIZE 512

//Descriptor
struct GiDesc{
    ResHandle handle;
    uint reset_flag;
    uvec2 pad0;

    vec3 position;
    float pad1;
    vec3 halfextent;
    float pad2;
};

///Calculates the lookup-index into the gi buffer 
uint hash_pos(GiDesc desc, vec3 pos, out bool is_invalid){

    is_invalid = false;
    //currently just offsetting into the gi space, and the n doing a tripple_lookup
    vec3 l = pos - desc.position;
    
    if (!all(lessThan(l, desc.halfextent)) || !all(lessThan(-desc.halfextent, l))){
        is_invalid = true;
        return 0;
    }

    //offset l int 0..(2*half_extent) range, then into 0..GI_CACHE_SIZE range, and finally
    //cast into a cell
    uvec3 lint = uvec3((l + desc.halfextent / vec3(GI_CACHE_SIZE) ));

    //now flatten the 3d lookup into a 1d index
    return lint.x * (GI_CACHE_SIZE * GI_CACHE_SIZE) + lint.y * GI_CACHE_SIZE + lint.z;    
}

struct GiCacheCell{
    // Last known normal value of the surface.
    // used for normal based rejection
    vec3 normal;
    // The alpha value used for blending
    float alpha;
    // Last known cache value
    vec3 radiance;
    // padding to 2X16 byte
    float pad1;
};

