#![cfg_attr(target_arch = "spirv", no_std)]
#![feature(asm_experimental_arch)]

#[cfg(target_arch = "spirv")]
use shared::spirv_std::num_traits::Float;

use shared::spirv_std::{self, Sampler};
use shared::spirv_std::{spirv, Image, RuntimeArray};
use spirv_std::glam::{Mat3, UVec2, UVec3, Vec3, Vec3Swizzles, Vec4, Vec4Swizzles};

fn rrt_and_odt_fit(v: Vec3) -> Vec3 {
    let a = v * (v + 0.0245786) - 0.000090537;
    let b = v * (0.983729 * v + 0.4329510) + 0.238081;
    a / b
}

///Fast aces based on this: https://github.com/TheRealMJP/BakingLab/blob/master/BakingLab/ACES.hlsl
pub fn aces_mapping(hdr: Vec4) -> Vec4 {
    /*
    const INPUT_MAT: Mat3 = Mat3::from_cols_array(&[
        0.59719, 0.35458, 0.04823, 0.07600, 0.90834, 0.01566, 0.02840, 0.13383, 0.83777,
    ]);

    const OUTPUT_MAT: Mat3 = Mat3::from_cols_array(&[
        1.60475, -0.53108, -0.07367, -0.10208, 1.10813, -0.00605, -0.00327, -0.07276, 1.07602,
    ]);
    */
    const INPUT_MAT: Mat3 = Mat3::from_cols_array(&[
        0.59719, 0.07600, 0.02840, 0.35458, 0.90834, 0.13383, 0.04823, 0.01566, 0.83777,
    ]);

    const OUTPUT_MAT: Mat3 = Mat3::from_cols_array(&[
        1.60475, -0.10208, -0.00327, -0.53108, 1.10813, -0.07276, -0.07367, -0.00605, 1.07602,
    ]);

    let col = rrt_and_odt_fit(INPUT_MAT * hdr.xyz());
    (OUTPUT_MAT * col)
        .clamp(Vec3::ZERO, Vec3::ONE)
        .extend(hdr.w)
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn hdrldr(
    #[spirv(push_constant)] push: &shared::HdrToLdrPush,
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(descriptor_set = 1, binding = 0)] rgbaf32_images: &RuntimeArray<
        Image!(2D, format = rgba32f, sampled = false),
    >,
    //For sampling the hdr image in the DOF pass
    #[spirv(descriptor_set = 2, binding = 0)] src_sampled_image: &RuntimeArray<
        Image!(2D, format = rgba32f, sampled = true),
    >,
    //single channel depth image
    #[spirv(descriptor_set = 2, binding = 0)] src_sampled_depth_image: &RuntimeArray<
        Image!(2D, format = r32f, sampled = true),
    >,
    //sampler source
    #[spirv(descriptor_set = 3, binding = 0)] sampler: &RuntimeArray<Sampler>,
) {
    let coord = id.xy();
    if coord.x >= push.resolution[0] || coord.y >= push.resolution[1] {
        return;
    }
    let coordf32 = coord.as_vec2();
    let coord_uv = coordf32 / UVec2::new(push.resolution[0], push.resolution[1]).as_vec2();

    let hdr_baseline = if push.hdr_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.hdr_image.index() as usize)
                .read(coord)
        }
    } else {
        Vec4::new(0.0, 0.0, 1.0, f32::INFINITY)
    };

    let ldr = aces_mapping(hdr_baseline);
    let gamma_corrected = ldr.powf(1.0 / push.gamma);
    if push.ldr_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.ldr_image.index() as usize)
                .write(coord, gamma_corrected);
        }
    }
}
