use crate::glam::Vec3;

use spirv_std::num_traits::Float;


pub fn d_ggx(n_dot_h: f32, roughness: f32) -> f32{
    let a = n_dot_h * roughness;
    let k = roughness / (1.0 - n_dot_h * n_dot_h + a * a);
    k * k * (1.0 / core::f32::consts::PI)
}


pub fn v_smith_ggx(n_dot_v: f32, n_dot_l: f32, roughness: f32) -> f32{
    let ggxv = n_dot_l * (n_dot_v * (1.0 - roughness) + roughness);
    let ggxl = n_dot_v * (n_dot_l * (1.0 - roughness) + roughness);
    0.5 / (ggxv + ggxl)
}


pub fn f_schlick(u: f32, f0: Vec3) -> Vec3{
    let f = (1.0 - u).powf(5.0);
    f + f0 * (1.0 - f)
}


pub fn fd_lambert() -> f32{
    1.0 / core::f32::consts::PI
}

pub fn f0(metallic: f32, albedo: Vec3) -> Vec3{    
    const REFLECTANCE: f32 = 0.05;
    0.16 * REFLECTANCE * REFLECTANCE * (1.0 - metallic) * albedo * metallic
}

#[allow(dead_code)]
fn f90(f0: Vec3) -> f32{
    //using a small hack here
    (f0.y * 50.0).clamp(0.0, 1.0)
}


pub fn brdf_std(v: Vec3, l: Vec3, n: Vec3, roughness: f32, metallic: f32, albedo: Vec3) -> Vec3{
    let diffuse_color = (1.0 - metallic) * albedo;
    let f0 = f0(metallic, albedo);
    // let v = v.normalize();
    // let l = l.normalize();
    let h = (v + l).normalize();

    let n_dot_v = n.dot(v).abs() + 0.00001;
    let n_dot_l = n.dot(l).clamp(0.0, 1.0);
    let n_dot_h = n.dot(h).clamp(0.0, 1.0);
    let l_dot_h = l.dot(h).clamp(0.0, 1.0);

    let roughness = roughness * roughness;
    let d = d_ggx(n_dot_h, roughness);
    let f = f_schlick(l_dot_h, f0);
    let v = v_smith_ggx(n_dot_v, n_dot_l, roughness);

    let f_specular = (d * v) * f;
    let f_diffuse = diffuse_color * fd_lambert();

    f_specular + f_diffuse
}
