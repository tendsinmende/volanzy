use spirv_std::glam::UVec2;
#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::Float;

use crate::gicache::{CACHE_CLUSTER_SIZE, CACH_CLUSTER_OFFSET};

const SPEC_CS: usize = 4096 * 4096;

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct SpecularCache {
    pub normal: [f32; 3],
    pub roughness: f32,
    pub specular: [f32; 3],
    pub variance: f32,
    pub pos: [f32; 3],
    pub metalness: f32,
}

impl SpecularCache {
    pub fn has_nan(&self) -> bool {
        self.normal[0].is_nan()
            || self.normal[1].is_nan()
            || self.normal[2].is_nan()
            || self.roughness.is_nan()
            || self.specular[0].is_nan()
            || self.specular[1].is_nan()
            || self.specular[2].is_nan()
            || self.variance.is_nan()
            || self.pos[0].is_nan()
            || self.pos[1].is_nan()
            || self.pos[2].is_nan()
            || self.metalness.is_nan()
    }
}

impl Default for SpecularCache {
    fn default() -> Self {
        SpecularCache {
            normal: [0.0; 3],
            roughness: 1.0,
            specular: [0.0; 3],
            variance: 1.0,
            pos: [0.0; 3],
            metalness: 0.0,
        }
    }
}

pub struct SpecularBuffer {
    pub cells: [SpecularCache; SPEC_CS],
}

impl SpecularBuffer {
    pub fn index_2d_to_linear(index: UVec2, resolution: UVec2) -> (usize, bool) {
        let clusters_per_line = (resolution.y / CACHE_CLUSTER_SIZE) + 1;
        //index within this cluster
        let in_cluster_index =
            UVec2::new(index.x % CACHE_CLUSTER_SIZE, index.y % CACHE_CLUSTER_SIZE);

        let cluster_offset =
            //How many whole lines we are offset
            clusters_per_line * CACH_CLUSTER_OFFSET * (index.x / CACHE_CLUSTER_SIZE)
            //How many clusters on this line we are offset
            + CACH_CLUSTER_OFFSET * (index.y / CACHE_CLUSTER_SIZE);

        //now lineraize the cluster index and add all together
        let in_cluster_linear = in_cluster_index.x * CACHE_CLUSTER_SIZE + in_cluster_index.y;
        let idx = (cluster_offset + in_cluster_linear) as usize;
        (idx, idx <= SPEC_CS)
    }
}
