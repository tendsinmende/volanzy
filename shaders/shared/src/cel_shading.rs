

use spirv_std::num_traits::Float;
use spirv_std::glam::Vec3;


pub fn cel_diffuse(n: Vec3, l: Vec3, albedo: Vec3, illuminance: Vec3) -> Vec3{
    let n_dot_l = n.normalize().dot(l.normalize()).clamp(0.0, 1.0);

    const D_STEPS: u32 = 5;
    
    let dot_step = (n_dot_l * (D_STEPS as f32)).floor() / (D_STEPS as f32);

    albedo * (illuminance * dot_step)
}


pub fn cel_specular(
    v: Vec3 , 
    l: Vec3, 
    n: Vec3, 
    metallic: f32, 
    roughness: f32, 
    albedo: Vec3
) -> Vec3{
    //let v = v * Vec3::new(1.0, -1.0, 1.0);
    let h = (v + l).normalize();
    let n_dot_h = n.dot(h).clamp(0.0, 1.0);

    let spec = n_dot_h.powf((1.0 - roughness) * 50.0).max(0.0001) * Vec3::ONE.lerp(albedo, metallic);
    const S_STEPS: f32 = 5.0;
    (spec * S_STEPS).floor() / S_STEPS
}
