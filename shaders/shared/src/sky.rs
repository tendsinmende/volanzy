
use core::f32::consts::PI;
use crate::spirv_std::glam::{vec3, Vec3};

use spirv_std::num_traits::Float;

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct DirLight {
    ///Worldspace direction of the light
    pub direction: [f32; 3],
    pub bias: f32,
    pub color: [f32; 3],
    pub intensity: f32,

    //Sky rendering variables
    pub depolarization_factor: f32,
    pub mie_coefficient: f32,
    pub mie_directional_g: f32,
    pub mie_v: f32,

    pub mie_k_coefficient: [f32; 3],
    pub mie_zenith_length: f32,

    pub primaries: [f32; 3],
    pub num_molecules: f32,

    pub rayleigh: f32,
    pub rayleigh_zenith_lenght: f32,
    pub refractive_index: f32,
    pub sun_angular_diameter_degrees: f32,

    pub sun_intensity_factor: f32,
    pub sun_intensity_falloff_steepness: f32,
    pub turbidity: f32,
    pub pad0: f32,
}

impl Default for DirLight {
    fn default() -> Self {
        DirLight {
            direction: [1.0, -1.0, 1.0],
            bias: 0.001,
            intensity: 3.0,
            color: [1.0; 3],

            depolarization_factor: 0.035,
            mie_coefficient: 0.005,
            mie_directional_g: 0.8,
            mie_k_coefficient: [0.686, 0.678, 0.666],
            mie_v: 4.0,
            mie_zenith_length: 1.25e3,
            num_molecules: 2.542e25,
            primaries: [6.8e-7, 5.5e-7, 4.5e-7],
            rayleigh: 1.0,
            rayleigh_zenith_lenght: 8.4e3,
            refractive_index: 1.0003,
            sun_angular_diameter_degrees: 0.0093333,
            sun_intensity_factor: 500.0,
            sun_intensity_falloff_steepness: 1.5,
            turbidity: 2.0,
            pad0: 0.0,
        }
    }
}


pub fn smoothstep(edge0: f32, edge1: f32, x: f32) -> f32 {
    // Scale, bias and saturate x to 0..1 range
    let x = ((x - edge0) / (edge1 - edge0)).clamp(0.0, 1.0);
    // Evaluate polynomial
    x * x * (3.0 - 2.0 * x)
}
fn total_rayleigh(sun: &DirLight, lambda: Vec3) -> Vec3 {
    (8.0 * PI.powf(3.0)
        * (sun.refractive_index.powf(2.0) - 1.0).powf(2.0)
        * (6.0 + 3.0 * sun.depolarization_factor))
        / (3.0 * sun.num_molecules * lambda.powf(4.0) * (6.0 - 7.0 * sun.depolarization_factor))
}

fn total_mie(sun: &DirLight, lambda: Vec3, k: Vec3, t: f32) -> Vec3 {
    let c = 0.2 * t * 10e-18;
    0.434 * c * PI * ((2.0 * PI) / lambda).powf(sun.mie_v - 2.0) * k
}

fn rayleigh_phase(cos_theta: f32) -> f32 {
    (3.0 / (16.0 * PI)) * (1.0 + cos_theta.powf(2.0))
}

fn henyey_greenstein_phase(cos_theta: f32, g: f32) -> f32 {
    (1.0 / (4.0 * PI)) * ((1.0 - g.powf(2.0)) / (1.0 - 2.0 * g * cos_theta + g.powf(2.0)).powf(1.5))
}

fn sun_intensity(sun: &DirLight, zenith_angle_cos: f32) -> f32 {
    let cutoff_angle = PI / 1.95; // Earth shadow hack
    sun.sun_intensity_factor
        * 0.0f32.max(
            1.0 - (-((cutoff_angle - (zenith_angle_cos)).acos()
                / sun.sun_intensity_falloff_steepness))
                .exp(),
        )
}

pub fn sky(sun: &DirLight, dir: Vec3) -> Vec3 {
    let sun_position = -1.0 * Vec3::from(sun.direction);
    let up = vec3(0.0, 1.0, 0.0);
    let sunfade = 1.0 - (1.0 - (sun_position.y / 450000.0).clamp(0.0, 1.0).exp());
    let rayleigh_coefficient = sun.rayleigh - (1.0 * (1.0 - sunfade));
    let beta_r = total_rayleigh(sun, Vec3::from(sun.primaries)) * rayleigh_coefficient;

    // Mie coefficient
    let beta_m = total_mie(
        sun,
        Vec3::from(sun.primaries),
        Vec3::from(sun.mie_k_coefficient),
        sun.turbidity,
    ) * sun.mie_coefficient;

    // Optical length, cutoff angle at 90 to avoid singularity
    let zenith_angle = (up.dot(dir).max(0.0)).acos();
    let denom = (zenith_angle).cos() + 0.15 * (93.885 - ((zenith_angle * 180.0) / PI)).powf(-1.253);

    let s_r = sun.rayleigh_zenith_lenght / denom;
    let s_m = sun.mie_zenith_length / denom;

    // Combined extinction factor
    let fex = (-(beta_r * s_r + beta_m * s_m)).exp();

    // In-scattering
    let sun_direction = sun_position.normalize();
    let cos_theta = dir.dot(sun_direction);
    let beta_r_theta = beta_r * rayleigh_phase(cos_theta * 0.5 + 0.5);

    let beta_m_theta = beta_m * henyey_greenstein_phase(cos_theta, sun.mie_directional_g);
    let sun_e = sun_intensity(sun, sun_direction.dot(up));
    let mut lin =
        (sun_e * ((beta_r_theta + beta_m_theta) / (beta_r + beta_m)) * (Vec3::splat(1.0) - fex))
            .powf(1.5);

    lin *= Vec3::splat(1.0).lerp(
        (sun_e * ((beta_r_theta + beta_m_theta) / (beta_r + beta_m)) * fex).powf(0.5),
        ((1.0 - up.dot(sun_direction)).powf(5.0)).clamp(0.0, 1.0),
    );

    // Composition + solar disc
    let sun_angular_diameter_cos = sun.sun_angular_diameter_degrees.cos();
    let sundisk = smoothstep(
        sun_angular_diameter_cos,
        sun_angular_diameter_cos + 0.00002,
        cos_theta,
    );
    let mut l0 = 0.1 * fex;
    l0 += sun_e * 19000.0 * fex * sundisk;

    lin + l0
}

//Fast skycolor described in 
// by Íñigo Quílez
// https://www.shadertoy.com/view/MdX3Rr
pub fn fast_sky(dir: Vec3, sun: &DirLight) -> Vec3{
    let sundot = dir.dot(Vec3::from(sun.direction).normalize()).clamp(0.0, 1.0);
    let mut col = Vec3::new(0.2, 0.5, 0.85) * 1.1 - dir.y.max(0.01) * dir.y.max(0.01) * 0.5;
    col = col.lerp(0.85 * Vec3::new(0.7, 0.75, 0.85), 1.0 - dir.y.max(0.0).powf(6.0));

    col += 0.25 * Vec3::new(1.0, 0.7, 0.4) * sundot.powf(5.0);
    col += 0.25 * Vec3::new(1.0, 0.8, 0.6) * sundot.powf(64.0);
    col += 0.25 * Vec3::new(1.0, 0.8, 0.6) * sundot.powf(512.0);

    col += ((0.1 - dir.y) * 10.0).clamp(0.0, 1.0) * Vec3::new(0.0, 0.1, 0.2);
    col += 0.2 * Vec3::new(1.0, 0.8, 0.6) * sundot.powf(8.0);
    
    col
}
