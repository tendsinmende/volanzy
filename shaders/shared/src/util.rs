use crate::glam::Vec3;

use spirv_std::{
    glam::{vec3, Quat, Vec2, Vec3Swizzles},
    num_traits::Float,
};

pub fn reflect(incident: Vec3, normal: Vec3) -> Vec3 {
    incident - 2.0 * normal.dot(incident) * normal
}

///Computs the exposure setting for ISO100
pub fn exposure(ev100: f32) -> f32 {
    1.0 / (2.0f32.powf(ev100) * 1.2)
}
///Returns the exposure value calculated from the camera exposure settings
pub fn ev100(aperture: f32, shutter_speed: f32, sensitivity: f32) -> f32 {
    ((aperture * aperture) / shutter_speed * 100.0 / sensitivity).log2()
}

pub fn default_camera_exposure() -> f32 {
    exposure(ev100(1.0 / 8.0, 1.0 / 100.0, 100.0))
}

pub fn rand(seed: Vec2) -> f32 {
    (seed.dot(Vec2::new(12.9898, 78.233)).sin() * 43758.5453).fract()
}

///Generates a random ray on unit hemisphere
pub fn dir_hemi(u: f32, v: f32) -> Vec3 {
    let radial = u.sqrt();
    let theta = 2.0 * core::f32::consts::PI * v;
    let x = radial * theta.cos();
    let y = radial * theta.sin();

    Vec3::new(x, -(1.0 - u).sqrt(), y)
}

pub fn dir_hemi_sampled(normal: Vec3, u: f32, v: f32) -> Vec3 {
    (normal + unit_vec(u, v)).normalize()
}

///Generates a random ray on a hemisphere on
///Oriented along `normal`
pub fn random_dir_hemi(normal: Vec3, state: &mut u32) -> Vec3 {
    (normal + random_unit_vec(state)).normalize()
}

pub fn wang_hash(seed: &mut u32) -> u32 {
    *seed = (*seed ^ 61) ^ *seed >> 16;
    *seed *= 9;
    *seed = *seed ^ (*seed >> 4);
    *seed *= 0x27d4eb2d;
    *seed = *seed ^ (*seed >> 15);
    *seed
}

pub fn random_float(state: &mut u32) -> f32 {
    (wang_hash(state) as f32) / (u32::MAX as f32)
}

const TWO_PI: f32 = 2.0 * core::f32::consts::PI;

pub fn unit_vec(u: f32, v: f32) -> Vec3 {
    let z = u * 2.0 - 1.0;
    let a = v * TWO_PI;
    let r = (1.0 - z * z).sqrt();
    let x = r * a.cos();
    let y = r * a.sin();
    Vec3::new(x, y, z)
}

pub fn random_unit_vec(state: &mut u32) -> Vec3 {
    let z = random_float(state) * 2.0 - 1.0;
    let a = random_float(state) * TWO_PI;
    let r = (1.0 - z * z).sqrt();
    let x = r * a.cos();
    let y = r * a.sin();
    Vec3::new(x, y, z)
}

pub fn lumiosity(color: Vec3) -> f32 {
    color.dot(vec3(0.2126, 0.7152, 0.0722))
}
