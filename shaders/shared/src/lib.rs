#![cfg_attr(target_arch = "spirv", no_std)]
#![feature(asm_experimental_arch)]

use core::f32::consts::PI;

use marpii_rmg_shared::ResourceHandle;

pub use spirv_std;
pub use spirv_std::glam;
use spirv_std::glam::{Quat, Vec2, Vec3};

pub mod cel_shading;
pub mod gicache;

pub mod pbr;
pub mod simple_shading;
pub mod sky;
pub mod spec_cache;
pub mod util;

use spirv_std::num_traits::Float;

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct CameraBundle {
    pub pos: [f32; 3],
    pub fov: f32,
    pub rotation: [f32; 4],
    pub exposure: f32,
    pub pad0: [f32; 3],
}

impl CameraBundle {
    fn aspect_ratio(&self, resolution: [u32; 2]) -> f32 {
        resolution[0] as f32 / resolution[1] as f32
    }

    pub fn camera_direction(&self) -> Vec3 {
        //now rotate for camera
        Quat::from_array(self.rotation)
            .mul_vec3(Vec3::Z)
            .normalize()
    }
    //ndc in -1.0 .. 1.0
    pub fn ray_from_ndc(&self, ndc: Vec2, resolution: [u32; 2]) -> Ray {
        let px = ndc.x * (self.fov / 2.0 * PI / 180.0).tan() * self.aspect_ratio(resolution);
        let py = ndc.y * (self.fov / 2.0 * PI / 180.0).tan();
        let direction = Vec3::new(px, py, 1.0);

        //now rotate for camera
        let direction = Quat::from_array(self.rotation).mul_vec3(direction);

        Ray {
            direction: direction.normalize(),
            max_t: 300.0,
            origin: self.pos.into(),
        }
    }
}

impl Default for CameraBundle {
    fn default() -> Self {
        CameraBundle {
            pos: [0.0, 0.0, 0.0],
            fov: 90.0,
            rotation: Quat::IDENTITY.into(),
            exposure: 1.0,
            pad0: [0.0; 3],
        }
    }
}

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct RenderUniform {
    pub camera: CameraBundle,
    pub resolution: [u32; 2],
    pub target_image: ResourceHandle,
    pub pad1: [u32; 2],
    pub offset: [f32; 3],
    pub time: f32,
}

impl Default for RenderUniform {
    fn default() -> Self {
        RenderUniform {
            camera: CameraBundle::default(),
            resolution: [100, 100],
            target_image: ResourceHandle::INVALID,
            pad1: [0; 2],
            offset: [0.0; 3],
            time: 0.0,
        }
    }
}

pub struct Ray {
    pub origin: Vec3,
    pub max_t: f32,
    pub direction: Vec3,
}

impl Ray {
    pub fn at(&self, t: f32) -> Vec3 {
        self.origin + self.direction * t
    }
}

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct HdrPush {
    pub camera: CameraBundle,
    pub resolution: [u32; 2],
    pub specular_image: ResourceHandle,
    pub irradiance_image: ResourceHandle,
    pub target_image: ResourceHandle,
    pub pad0: [u32; 3],
    pub offset: [f32; 3],
    pub time: f32,
}

impl Default for HdrPush {
    fn default() -> Self {
        HdrPush {
            camera: CameraBundle::default(),
            resolution: [100, 100],
            specular_image: ResourceHandle::INVALID,
            irradiance_image: ResourceHandle::INVALID,
            target_image: ResourceHandle::INVALID,
            pad0: [0; 3],
            offset: [0.0; 3],
            time: 0.0,
        }
    }
}

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct HdrToLdrPush {
    pub resolution: [u32; 2],
    pub hdr_image: ResourceHandle,
    pub linear_sampler: ResourceHandle,
    pub depth_image: ResourceHandle,
    pub ldr_image: ResourceHandle,
    pub pad0: [u32; 2],
    pub gamma: f32,
    pub pad1: [f32; 3],
}
impl Default for HdrToLdrPush {
    fn default() -> Self {
        HdrToLdrPush {
            resolution: [100, 100],
            hdr_image: ResourceHandle::INVALID,
            linear_sampler: ResourceHandle::INVALID,
            depth_image: ResourceHandle::INVALID,
            ldr_image: ResourceHandle::INVALID,
            pad0: [0; 2],
            gamma: 2.2,
            pad1: [0.0; 3],
        }
    }
}

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct SpecularPush {
    pub camera: CameraBundle,
    pub resolution: [u32; 2],
    pub target_image: ResourceHandle,
    pub specular_cache: ResourceHandle,

    pub depth_image: ResourceHandle,
    pub pad0: [u32; 3],

    pub offset: [f32; 3],
    pub time: f32,

    pub spec_denoise_sigma_normal: f32,
    pub spec_deniose_sigma_pos: f32,
    pub spec_denoise_sigma_luminance: f32,
    pub pad1: f32,
}

impl Default for SpecularPush {
    fn default() -> Self {
        SpecularPush {
            camera: CameraBundle::default(),
            resolution: [100, 100],
            target_image: ResourceHandle::INVALID,
            specular_cache: ResourceHandle::INVALID,
            depth_image: ResourceHandle::INVALID,
            pad0: [0; 3],
            offset: [0.0; 3],
            time: 0.0,

            spec_deniose_sigma_pos: 1.0,
            spec_denoise_sigma_normal: 128.0,
            spec_denoise_sigma_luminance: 4.0,
            pad1: 0.0,
        }
    }
}

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct IrradiancePush {
    pub camera: CameraBundle,
    pub resolution: [u32; 2],
    pub target_image: ResourceHandle,
    pub gi_cache: ResourceHandle,
    pub depth_image: ResourceHandle,
    pub pad0: [u32; 3],

    pub offset: [f32; 3],
    pub time: f32,

    pub gi_halfext: [f32; 3],
    pub gi_max_normal_div: f32,
    pub gi_pos: [f32; 3],
    pub gi_max_pos_div: f32,

    pub gi_denoise_sigma_normal: f32,
    pub gi_deniose_sigma_pos: f32,
    pub gi_denoise_sigma_luminance: f32,
    pub pad1: f32,
}
impl Default for IrradiancePush {
    fn default() -> Self {
        IrradiancePush {
            camera: CameraBundle::default(),
            resolution: [100, 100],
            target_image: ResourceHandle::INVALID,
            gi_cache: ResourceHandle::INVALID,
            depth_image: ResourceHandle::INVALID,
            pad0: [0; 3],

            offset: [0.0; 3],
            time: 0.0,

            gi_halfext: [GI_VOL_SIZE; 3],
            gi_max_normal_div: 0.5,
            gi_pos: [0.0; 3],
            gi_max_pos_div: 0.5,

            gi_deniose_sigma_pos: 1.0,
            gi_denoise_sigma_normal: 128.0,
            gi_denoise_sigma_luminance: 4.0,
            pad1: 0.0,
        }
    }
}

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct GiTracePush {
    pub camera: CameraBundle,

    pub resolution: [u32; 2],
    pub gi_cache: ResourceHandle,
    pub depth_image: ResourceHandle,

    pub bluenoise: ResourceHandle,
    pub noise_sampler: ResourceHandle,
    pub epoch: u32,
    pub bounces: u32,

    pub offset: [f32; 3],
    pub time: f32,

    pub reset_gi: u32,
    pub bluenoise_ext: [u32; 3],

    pub gi_max_trace: f32,
    pub gi_carry_over_weight: f32,
    pub trace_cache_depth_reject_mul: f32,
    pub trace_cache_age_add: f32,
    pub trace_cache_age_max: f32,
    pub trace_cache_normal_reject: f32,
}

pub const GI_VOL_SIZE: f32 = 100.0;
pub const GI_CACHE_EXT: usize = 256;
pub const GI_CACHE_SIZE: usize = GI_CACHE_EXT * GI_CACHE_EXT * GI_CACHE_EXT;

impl Default for GiTracePush {
    fn default() -> Self {
        GiTracePush {
            camera: CameraBundle::default(),
            resolution: [100, 100],
            gi_cache: ResourceHandle::INVALID,
            depth_image: ResourceHandle::INVALID,
            bluenoise: ResourceHandle::INVALID,
            noise_sampler: ResourceHandle::INVALID,
            epoch: 0,
            bluenoise_ext: [0; 3],
            offset: [0.0; 3],
            time: 0.0,
            reset_gi: 1,
            bounces: 3,

            gi_max_trace: 25.0,
            gi_carry_over_weight: 0.9,
            trace_cache_depth_reject_mul: 0.25,
            trace_cache_age_add: 0.1,
            trace_cache_age_max: 1.0,
            trace_cache_normal_reject: 0.0,
        }
    }
}
#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct SpecularTracePush {
    pub camera: CameraBundle,

    pub resolution: [u32; 2],
    pub specular_cache: ResourceHandle,
    pub depth_image: ResourceHandle,

    pub bluenoise: ResourceHandle,
    pub noise_sampler: ResourceHandle,
    pub epoch: u32,
    pub bounces: u32,

    pub offset: [f32; 3],
    pub time: f32,

    pub reset_specular: u32,
    pub bluenoise_ext: [u32; 3],

    pub specular_max_trace: f32,
    pub specular_carry_epsilon: f32,
    pub specular_roughness_recject: f32,
    pub trace_cache_postion_reject: f32,

    pub trace_cache_normal_reject: f32,
    pub pad0: [f32; 3],
}
impl Default for SpecularTracePush {
    fn default() -> Self {
        SpecularTracePush {
            camera: CameraBundle::default(),
            resolution: [100, 100],
            specular_cache: ResourceHandle::INVALID,
            depth_image: ResourceHandle::INVALID,
            bluenoise: ResourceHandle::INVALID,
            noise_sampler: ResourceHandle::INVALID,
            epoch: 0,
            bluenoise_ext: [0; 3],
            offset: [0.0; 3],
            time: 0.0,
            reset_specular: 1,
            bounces: 3,

            specular_max_trace: 100.0,
            specular_carry_epsilon: 0.95,
            specular_roughness_recject: 0.1,
            trace_cache_postion_reject: 1.0,
            trace_cache_normal_reject: 0.0,
            pad0: [0.0; 3],
        }
    }
}
