use spirv_std::glam::UVec2;

use crate::Vec3;

#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::Float;

pub const GI_VOL_SIZE: f32 = 100.0;
pub const GI_CACHE_EXT: usize = 256;
pub const GI_CACHE_SIZE: usize = GI_CACHE_EXT * GI_CACHE_EXT;

#[cfg_attr(not(target_arch = "spirv"), derive(Clone, Copy, Debug))]
#[cfg_attr(target_arch = "spirv", derive(Clone, Copy))]
#[repr(C, align(16))]
pub struct GiCache {
    pub normal: [f32; 3],
    pub age: f32,
    pub radiance: [f32; 3],
    //Tracks variance of current and previouse samples.
    pub variance: f32,
    pub pos: [f32; 3],
    pad3: f32,
}

impl Default for GiCache {
    fn default() -> Self {
        GiCache {
            normal: [0.0; 3],
            age: 0.0,
            radiance: [0.0; 3],
            variance: 1.0,
            pos: [0.0; 3],
            pad3: 0.0,
        }
    }
}

pub const GI_CS: usize = 4096 * 4096;
pub const CACHE_CLUSTER_SIZE: u32 = 8;
pub const CACH_CLUSTER_OFFSET: u32 = CACHE_CLUSTER_SIZE * CACHE_CLUSTER_SIZE;
pub struct GiBuffer {
    pub cells: [GiCache; GI_CS],
}

pub struct GiCacheDesc {
    pub position: Vec3,
    pub halfextent: Vec3,
}

impl GiCacheDesc {
    pub fn index_2d_to_linear(index: UVec2, resolution: UVec2) -> (usize, bool) {
        let clusters_per_line = (resolution.y / CACHE_CLUSTER_SIZE) + 1;
        //index within this cluster
        let in_cluster_index =
            UVec2::new(index.x % CACHE_CLUSTER_SIZE, index.y % CACHE_CLUSTER_SIZE);

        let cluster_offset =
            //How many whole lines we are offset
            clusters_per_line * CACH_CLUSTER_OFFSET * (index.x / CACHE_CLUSTER_SIZE)
            //How many clusters on this line we are offset
            + CACH_CLUSTER_OFFSET * (index.y / CACHE_CLUSTER_SIZE);

        //now lineraize the cluster index and add all together
        let in_cluster_linear = in_cluster_index.x * CACHE_CLUSTER_SIZE + in_cluster_index.y;
        let idx = (cluster_offset + in_cluster_linear) as usize;
        (idx, idx <= GI_CS)
    }

    //FOV cone based cell-size calculation
    pub fn cell_size(depth: f32, fov: f32) -> f32 {
        (fov.to_radians()).tan() * depth
    }
}
