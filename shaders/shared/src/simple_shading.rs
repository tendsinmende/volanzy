use spirv_std::glam::Vec3;
use spirv_std::num_traits::Float;

pub fn diffuse(n: Vec3, l: Vec3, albedo: Vec3, illuminance: Vec3) -> Vec3 {
    let n_dot_l = n.normalize().dot(l.normalize()).clamp(0.0, 1.0);
    albedo * illuminance * n_dot_l
}

pub fn specular(v: Vec3, l: Vec3, n: Vec3, metallic: f32, roughness: f32, albedo: Vec3) -> Vec3 {
    //let v = v * Vec3::new(1.0, -1.0, 1.0);
    let h = (v + l).normalize();
    let n_dot_h = n.dot(h).clamp(0.0, 1.0);

    let spec =
        n_dot_h.powf((1.0 - roughness) * 50.0).max(0.0001) * Vec3::ONE.lerp(albedo, metallic);
    spec
}
