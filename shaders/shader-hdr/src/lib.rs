#![cfg_attr(target_arch = "spirv", no_std)]
#![feature(asm_experimental_arch)]

use shared::glam::vec3;
use shared::spirv_std;
#[cfg(target_arch = "spirv")]
use shared::spirv_std::num_traits::Float;
use shared::spirv_std::{spirv, Image, RuntimeArray};
use spirv_std::glam::{UVec2, UVec3, Vec3, Vec3Swizzles, Vec4, Vec4Swizzles};

//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_sdf"]
pub fn eval_sdf(pos: Vec3, offset: Vec3, time: f32) -> f32 {
    let dist = (pos - offset).length() - (1.0 * time.sin());
    dist
}
//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_albedo"]
pub fn eval_albedo(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}
//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_mro"]
pub fn eval_mro(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}

//Uses the thetrahedron technique described here: https://iquilezles.org/articles/normalsSDF/
fn calc_normal(at: Vec3, offset: Vec3, time: f32) -> Vec3 {
    const H: f32 = 0.00001;
    (vec3(1.0, -1.0, -1.0) * eval_sdf(at + (H * vec3(1.0, -1.0, -1.0)), offset, time)
        + vec3(-1.0, -1.0, 1.0) * eval_sdf(at + (H * vec3(-1.0, -1.0, 1.0)), offset, time)
        + vec3(-1.0, 1.0, -1.0) * eval_sdf(at + (H * vec3(-1.0, 1.0, -1.0)), offset, time)
        + vec3(1.0, 1.0, 1.0) * eval_sdf(at + (H * vec3(1.0, 1.0, 1.0)), offset, time))
    .normalize()
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn entry_hdr(
    #[spirv(push_constant)] push: &shared::HdrPush,
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(descriptor_set = 1, binding = 0)] rgbaf32_images: &RuntimeArray<
        Image!(2D, format = rgba32f, sampled = false),
    >,
) {
    let coord = id.xy();
    if coord.x >= push.resolution[0] || coord.y >= push.resolution[1] {
        return;
    }
    let coordf32 = coord.as_vec2();
    let coord_uv = coordf32 / UVec2::new(push.resolution[0], push.resolution[1]).as_vec2();

    let ndc = coord_uv * 2.0 - 1.0;
    let ray = push.camera.ray_from_ndc(ndc, push.resolution);

    let specular_dist = if push.specular_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.specular_image.index() as usize)
                .read(coord)
        }
    } else {
        Vec4::new(0.0, 0.0, 0.0, f32::INFINITY)
    };

    let irradiance_pad = if push.irradiance_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.irradiance_image.index() as usize)
                .read(coord)
        }
    } else {
        Vec4::new(0.0, 0.0, 0.0, f32::INFINITY)
    };

    let specular = specular_dist.xyz();
    let irradiance = irradiance_pad.xyz();
    let depth = specular_dist.w;

    if depth == f32::INFINITY {
        if push.target_image.is_valid() {
            let sky =
                shared::sky::fast_sky(Vec3::from(ray.direction), &shared::sky::DirLight::default());
            unsafe {
                rgbaf32_images
                    .index(push.target_image.index() as usize)
                    .write(coord, sky.extend(1.0));
            }
        }
        return;
    }

    let pos = ray.at(depth);
    let mro = eval_mro(pos, push.offset.into(), push.time);
    let nrm = calc_normal(pos, push.offset.into(), push.time);
    let albedo = eval_albedo(pos, push.offset.into(), push.time);
    let light = shared::sky::DirLight::default();

    let diffuse = (1.0 - mro.x) * irradiance;

    /*
    let direct_specular = shared::simple_shading::specular(
        -ray.direction.normalize(),
        Vec3::from(light.direction),
        nrm,
        mro.x,
        mro.y,
        albedo,
    );
    */
    let hdr = irradiance + specular;
    if push.target_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.target_image.index() as usize)
                .write(coord, hdr.extend(1.0));
        }
    }
}
