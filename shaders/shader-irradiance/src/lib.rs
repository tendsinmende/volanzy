#![cfg_attr(target_arch = "spirv", no_std)]
#![feature(asm_experimental_arch)]

use shared::gicache::{GiBuffer, GiCache};
use shared::glam::IVec2;
#[cfg(target_arch = "spirv")]
use shared::spirv_std::num_traits::Float;
use shared::spirv_std::{self, ByteAddressableBuffer};
use shared::spirv_std::{spirv, Image, RuntimeArray, TypedBuffer};
use shared::{gicache::GiCacheDesc, glam::vec3};
use spirv_std::glam::{UVec2, UVec3, Vec3, Vec3Swizzles, Vec4Swizzles};

//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_sdf"]
pub fn eval_sdf(pos: Vec3, offset: Vec3, time: f32) -> f32 {
    let dist = (pos - offset).length() - (1.0 * time.sin());
    dist
}
//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_albedo"]
pub fn eval_albedo(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}
//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_mro"]
pub fn eval_mro(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}

//Uses the thetrahedron technique described here: https://iquilezles.org/articles/normalsSDF/
fn calc_normal(at: Vec3, offset: Vec3, time: f32) -> Vec3 {
    const H: f32 = 0.00001;
    (vec3(1.0, -1.0, -1.0) * eval_sdf(at + (H * vec3(1.0, -1.0, -1.0)), offset, time)
        + vec3(-1.0, -1.0, 1.0) * eval_sdf(at + (H * vec3(-1.0, -1.0, 1.0)), offset, time)
        + vec3(-1.0, 1.0, -1.0) * eval_sdf(at + (H * vec3(-1.0, 1.0, -1.0)), offset, time)
        + vec3(1.0, 1.0, 1.0) * eval_sdf(at + (H * vec3(1.0, 1.0, 1.0)), offset, time))
    .normalize()
}

fn trace_direct_light(ray: shared::Ray, smoothness: f32, push: &shared::IrradiancePush) -> f32 {
    let mut res: f32 = 1.0;
    let mut t = 0.01;
    for _ in 0..256 {
        let h = eval_sdf(ray.at(t), Vec3::from(push.offset), push.time);
        res = res.min(h / (smoothness * t));
        t += h.clamp(0.005, 0.5);
        if res < -1.0 || t > ray.max_t {
            break;
        }
    }
    res = res.max(-1.0);
    0.25 * (1.0 + res) * (1.0 + res) * (2.0 - res)
}

const EPSILON: f32 = 0.00001;
const GAUS_KERNEL: [f32; 9] = [
    1.0 / 16.0,
    1.0 / 8.0,
    1.0 / 16.0,
    1.0 / 8.0,
    1.0 / 4.0,
    1.0 / 8.0,
    1.0 / 16.0,
    1.0 / 8.0,
    1.0 / 16.0,
];

const WAVELET_H: [f32; 25] = [
    1.0 / 256.0,
    1.0 / 64.0,
    3.0 / 128.0,
    1.0 / 64.0,
    1.0 / 256.0,
    1.0 / 64.0,
    1.0 / 16.0,
    3.0 / 32.0,
    1.0 / 16.0,
    1.0 / 64.0,
    3.0 / 128.0,
    3.0 / 32.0,
    9.0 / 64.0,
    3.0 / 32.0,
    3.0 / 128.0,
    1.0 / 64.0,
    1.0 / 16.0,
    3.0 / 32.0,
    1.0 / 16.0,
    1.0 / 64.0,
    1.0 / 256.0,
    1.0 / 64.0,
    3.0 / 128.0,
    1.0 / 64.0,
    1.0 / 256.0,
];
///Samples the GIBuffer spacially rejecting samples that are
/// _too different_ based on the normal and position criteria
fn denoised_gi(
    gicache: &TypedBuffer<GiBuffer>,
    coord: UVec2,
    push: &shared::IrradiancePush,
) -> Vec3 {
    let (mididx, is_valid) = GiCacheDesc::index_2d_to_linear(coord, push.resolution.into());
    if !is_valid {
        //Sampling outside does not work
        return Vec3::ZERO;
    }
    let mid_cell = gicache.cells[mididx];
    let local_variance = mid_cell.variance;
    let mid_luminance = shared::util::lumiosity(mid_cell.radiance.into());

    //Load the middle cell, always, with max weight
    let mut res: Vec3 = Vec3::ZERO;
    let mut accweight = 0.0;
    const KERNEL_SIZE: i32 = 2;

    for x in -KERNEL_SIZE..=KERNEL_SIZE {
        for y in -KERNEL_SIZE..=KERNEL_SIZE {
            //Potential weight of the sample
            let sample_coord = coord.as_ivec2() + IVec2::new(x, y);
            let (sample_idx, is_sample_valid) =
                GiCacheDesc::index_2d_to_linear(sample_coord.as_uvec2(), push.resolution.into());
            if !is_sample_valid {
                continue;
            }

            //load sample
            let sample_cell = gicache.cells[sample_idx];

            let t = Vec3::from(mid_cell.pos) - Vec3::from(sample_cell.pos);
            let dist2 = t.dot(t);
            //weight of the postional component
            let wp = (-(dist2) / push.gi_deniose_sigma_pos).exp().min(1.0);
            //weight of the normal component.
            let wn = Vec3::from(sample_cell.normal)
                .dot(mid_cell.normal.into())
                .max(0.0)
                .powf(push.gi_denoise_sigma_normal);

            //TODO: prefilter with gaus3 instead of doing that here
            let mut gvl = 0.0001;
            for gy in -1..1 {
                for gx in -1..1 {
                    let gaus_cell_sample = sample_coord + IVec2::new(gx, gy);
                    let (gaus_cell_idx, is_gaus_valid) = GiCacheDesc::index_2d_to_linear(
                        gaus_cell_sample.as_uvec2(),
                        push.resolution.into(),
                    );
                    if !is_gaus_valid {
                        continue;
                    }

                    let lgv = gicache.cells[gaus_cell_idx].variance;
                    gvl += GAUS_KERNEL[(gx + 3 * gy + 4) as usize] * lgv;
                }
            }

            //After sampling the variance values, create the lumiosity rejection
            let local_luminance = shared::util::lumiosity(sample_cell.radiance.into());
            let wl = (-((mid_luminance - local_luminance).abs())
                / (push.gi_denoise_sigma_luminance * gvl.sqrt() + EPSILON))
                .exp()
                .min(1.0);

            let w = wp * wn * wl;

            let weight = WAVELET_H[(5 * (y + KERNEL_SIZE) + x + KERNEL_SIZE) as usize] * w;

            res += Vec3::from(sample_cell.radiance) * weight;
            accweight += weight;
        }
    }

    res / accweight
}

fn ao(pos: Vec3, normal: Vec3, push: &shared::IrradiancePush) -> f32 {
    const AO_RAD: f32 = 0.1;
    let h = eval_sdf(pos + normal * AO_RAD, Vec3::from(push.offset), push.time);

    (h / AO_RAD).clamp(0.0, 1.0)
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn main(
    #[spirv(push_constant)] push: &shared::IrradiancePush,
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(descriptor_set = 1, binding = 0)] rgbaf32_images: &RuntimeArray<
        Image!(2D, format = rgba32f, sampled = false),
    >,
    #[spirv(descriptor_set = 0, binding = 0, storage_buffer)] gicache: &mut RuntimeArray<
        TypedBuffer<GiBuffer>,
    >,
) {
    let coord = id.xy();
    if coord.x >= push.resolution[0] || coord.y >= push.resolution[1] {
        return;
    }
    let coordf32 = coord.as_vec2();
    let coord_uv = coordf32 / UVec2::new(push.resolution[0], push.resolution[1]).as_vec2();

    //reconstruct the ray position from depth
    let ndc = coord_uv * 2.0 - 1.0;
    let ray = push.camera.ray_from_ndc(ndc, push.resolution);

    let depth = if push.depth_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.depth_image.index() as usize)
                .read(coord)
                .x
        }
    } else {
        0.0
    };

    let pos = ray.at(depth);

    // //sample at position
    // let givalue = if push.gi_cache.is_valid() {
    //     //sample cache line
    //     // let gidesc = push.into_cache_desc();
    //     let (cindex, is_valid) = GiCacheDesc::index_2d_to_linear(coord, push.resolution.into());
    //     if is_valid {
    //         Vec3::from(unsafe {
    //             gicache.index_mut(push.gi_cache.index() as usize).cells[cindex].radiance
    //         })
    //     } else {
    //         Vec3::Y
    //     }
    // } else {
    //     Vec3::new(0.0, 0.0, 1.0)
    // };

    let givalue = denoised_gi(
        unsafe { &gicache.index(push.gi_cache.index() as usize) },
        coord,
        push,
    );

    let mro = eval_mro(pos, push.offset.into(), push.time);
    let albedo = eval_albedo(pos, push.offset.into(), push.time);
    let nrm = calc_normal(pos, push.offset.into(), push.time);
    let light = shared::sky::DirLight::default();

    let light_ray = shared::Ray {
        origin: pos,
        max_t: 20.0,
        direction: Vec3::from(light.direction),
    };

    let n_dot_l = light_ray.direction.normalize().dot(nrm);
    let shadow = if n_dot_l > 0.0 {
        trace_direct_light(light_ray, 1.0, &push).min(n_dot_l)
    } else {
        0.0
    };

    let ao = ao(pos, nrm, push);
    let light_power = Vec3::from(light.color) * light.intensity;
    //Calculate how the light _would_ reacts on the surface

    let diffuse_color = shared::pbr::fd_lambert() * albedo;
    let direct_light = shared::simple_shading::diffuse(
        nrm,
        light.direction.into(),
        diffuse_color,
        light_power * shadow * ao,
    );
    let radiance = (givalue * diffuse_color) + direct_light;
    //exposure correct radiance immediatly
    let radiance = radiance * push.camera.exposure;
    if push.target_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.target_image.index() as usize)
                .write(coord, radiance.extend(1.0));
        }
    }
}
