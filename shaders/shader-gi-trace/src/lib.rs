#![cfg_attr(target_arch = "spirv", no_std)]
#![feature(asm_experimental_arch)]

use shared::gicache::{GiBuffer, GiCache, GiCacheDesc};
use shared::glam::{vec3, UVec4, Vec2, Vec4};
use shared::sky::{fast_sky, DirLight};
use shared::util::{dir_hemi_sampled, random_dir_hemi};
use spirv_std::glam::{UVec2, UVec3, Vec3, Vec3Swizzles};
#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::Float;
use spirv_std::{self, Sampler};
use spirv_std::{spirv, Image, RuntimeArray, TypedBuffer};

const NUM_GI_TRACES: usize = 1;

pub fn map_bluenoise_sample(sample: UVec4) -> Vec4 {
    //map each channel down to 0..1
    sample.as_vec4() / Vec4::splat(u32::MAX as f32)
}

//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_sdf"]
pub fn eval_sdf(pos: Vec3, offset: Vec3, time: f32) -> f32 {
    let dist = (pos - offset).length() - (1.0 * time.sin());
    dist
}
//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_albedo"]
pub fn eval_albedo(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}
//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_mro"]
pub fn eval_mro(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}

//Uses the thetrahedron technique described here: https://iquilezles.org/articles/normalsSDF/
fn calc_normal(at: Vec3, offset: Vec3, time: f32) -> Vec3 {
    const H: f32 = 0.00001;
    (vec3(1.0, -1.0, -1.0) * eval_sdf(at + (H * vec3(1.0, -1.0, -1.0)), offset, time)
        + vec3(-1.0, -1.0, 1.0) * eval_sdf(at + (H * vec3(-1.0, -1.0, 1.0)), offset, time)
        + vec3(-1.0, 1.0, -1.0) * eval_sdf(at + (H * vec3(-1.0, 1.0, -1.0)), offset, time)
        + vec3(1.0, 1.0, 1.0) * eval_sdf(at + (H * vec3(1.0, 1.0, 1.0)), offset, time))
    .normalize()
}

struct HitInfo {
    albedo: Vec3,
    t: f32,
    nrm: Vec3,
}

fn trace_ray(ray: &shared::Ray, push: &shared::GiTracePush) -> HitInfo {
    //Short ray, at max 32 iterations
    let mut hit = HitInfo {
        albedo: Vec3::ZERO,
        t: f32::INFINITY,
        nrm: -Vec3::Y,
    };

    let mut t = 0.05;
    for _ in 0..32 {
        let r = eval_sdf(ray.at(t), push.offset.into(), push.time);

        if r < 0.01 {
            let albedo = eval_albedo(ray.at(t), push.offset.into(), push.time);
            let nrm = calc_normal(ray.at(t), push.offset.into(), push.time);
            hit = HitInfo { albedo, nrm, t };

            break;
        }

        t += r.max(0.05);
        if t > push.gi_max_trace {
            break;
        }
    }

    hit
}

fn trace_indirect_light(mut ray: shared::Ray, push: &shared::GiTracePush, rnd: &mut u32) -> Vec3 {
    let mut ret = Vec3::ZERO;
    let mut throughput = Vec3::ONE;

    for _b in 0..push.bounces {
        let hit = trace_ray(&ray, push);
        if hit.t == f32::INFINITY {
            //add sky and exit
            ret += fast_sky(ray.direction, &DirLight::default());
            break;
        } else {
            //has hit, therfore update ray and result, then bounce
            let mut hitpos = ray.at(hit.t);
            //offset to avoid self intersection.
            hitpos += hit.nrm * 0.1;
            ray.origin = hitpos;
            ray.direction = random_dir_hemi(hit.nrm, rnd);
            //Update albedo and throughput
            ret += hit.albedo * throughput;
            throughput *= hit.albedo;
        }
    }

    ret
}

fn sample_noise(
    image: &Image!(3D, format = rgba32ui, sampled = true),
    sampler: &Sampler,
    coord: UVec2,
    idx: u32,
) -> Vec4 {
    let src_coord = (coord % UVec2::splat(64)).as_vec2() / Vec2::splat(64.0);
    //add index to coord
    let src_coord = src_coord.extend((idx % 64) as f32 / 64.0);
    //Now sample and return

    let src = image.sample_by_lod(*sampler, src_coord, 0.0);

    //is encoded in 32bit, but actually just 16bit.
    src.as_vec4() / Vec4::splat((1 << 18) as f32)
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn gi_trace(
    #[spirv(push_constant)] push: &shared::GiTracePush,
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(descriptor_set = 1, binding = 0)] rgbaf32_images: &RuntimeArray<
        Image!(2D, format = rgba32f, sampled = false),
    >,
    #[spirv(descriptor_set = 2, binding = 0)] rgbau32_sampled: &RuntimeArray<
        Image!(3D, format = rgba32ui, sampled = true),
    >,
    #[spirv(descriptor_set = 0, binding = 0, storage_buffer)] gicache: &mut RuntimeArray<
        TypedBuffer<GiBuffer>,
    >,
    #[spirv(descriptor_set = 3, binding = 0)] sampler: &RuntimeArray<Sampler>,
) {
    if push.gi_cache.is_invalid() || push.bluenoise.is_invalid() || push.noise_sampler.is_invalid()
    {
        return;
    }
    let coord = id.xy();
    if coord.x >= push.resolution[0] || coord.y >= push.resolution[1] {
        return;
    }
    let coordf32 = coord.as_vec2();
    let coord_uv = coordf32 / UVec2::new(push.resolution[0], push.resolution[1]).as_vec2();

    //reconstruct the ray position from depth
    let ndc = coord_uv * 2.0 - 1.0;
    let ray = push.camera.ray_from_ndc(ndc, push.resolution);

    let depth = if push.depth_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.depth_image.index() as usize)
                .read(coord)
                .x
        }
    } else {
        0.0
    };

    let pos = ray.at(depth);
    let (cindex, is_valid) = GiCacheDesc::index_2d_to_linear(coord, push.resolution.into());

    if !is_valid {
        return;
    }

    let nrm = calc_normal(pos, push.offset.into(), push.time);

    let noise_sampler = unsafe { sampler.index(push.noise_sampler.index() as usize) };
    let src = unsafe { rgbau32_sampled.index(push.bluenoise.index() as usize) };
    let noise_epoch_shift = coord / UVec2::splat(64);
    let noise = sample_noise(src, noise_sampler, coord, push.epoch + noise_epoch_shift.x);

    let mut state = (noise.z * u32::MAX as f32) as u32;

    // let mut res = Vec3::ZERO;
    // for _i in 0..NUM_GI_TRACES {
    //     let dir = dir_hemi_sampled(nrm, noise.x, noise.y);
    //     res += trace_indirect_light(
    //         shared::Ray {
    //             origin: pos + nrm * 0.1,
    //             max_t: 100.0,
    //             direction: dir,
    //         },
    //         &push,
    //         &mut state,
    //     )
    // }

    let mut res = Vec3::ZERO;
    let dir = dir_hemi_sampled(nrm, noise.x, noise.y);
    res += trace_indirect_light(
        shared::Ray {
            origin: pos + nrm * 0.1,
            max_t: push.gi_max_trace,
            direction: dir,
        },
        &push,
        &mut state,
    );
    //Always add direct light
    // let dlr = shared::Ray {
    //     origin: pos + nrm * 0.1,
    //     max_t: 25.0,
    //     direction: random_dir_hemi(nrm, &mut state),
    // };
    // res += trace_direct_light(&dlr, 10.0, push)
    //     * fast_sky(dlr.direction, &DirLight::default())
    //     * nrm.dot(dlr.direction);

    //load cell
    let mut cell =
        unsafe { gicache.index_mut(push.gi_cache.index() as usize).cells[cindex as usize] };

    let cell_distance = (Vec3::from(cell.pos) - pos).length();
    let is_old_valid = Vec3::from(cell.normal).dot(nrm) > push.trace_cache_normal_reject
        // && cell_distance < GiCacheDesc::cell_size(depth, push.camera.fov);
        && cell_distance < depth * push.trace_cache_depth_reject_mul
            && push.reset_gi == 0;

    if is_old_valid {
        let old_lumiosity = shared::util::lumiosity(cell.radiance.into());
        let this_lumiosity = shared::util::lumiosity(res);
        //reuse old radiance value
        cell.radiance = (Vec3::from(cell.radiance) * (push.gi_carry_over_weight)
            + res * (1.0 - push.gi_carry_over_weight))
            .into();
        cell.age = (cell.age + push.trace_cache_age_add).clamp(0.0, push.trace_cache_age_max);
        cell.variance = (old_lumiosity - this_lumiosity).abs();
    } else {
        //set new
        cell.normal = nrm.into();
        cell.pos = pos.into();
        cell.radiance = res.into();
        cell.age = push.trace_cache_age_add;
        //set variance to _normal_
        cell.variance = 1.0;
    }

    unsafe {
        gicache.index_mut(push.gi_cache.index() as usize).cells[cindex as usize] = cell;
    };
}
