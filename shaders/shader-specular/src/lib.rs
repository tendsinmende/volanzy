#![cfg_attr(target_arch = "spirv", no_std)]
#![feature(asm_experimental_arch)]

use shared::glam::IVec2;
use shared::spec_cache::{self, SpecularBuffer};
use shared::spirv_std;
#[cfg(target_arch = "spirv")]
use shared::spirv_std::num_traits::Float;
use shared::spirv_std::{spirv, Image, RuntimeArray};
use shared::{glam::vec3, spirv_std::TypedBuffer};
use spirv_std::glam::{UVec2, UVec3, Vec3, Vec3Swizzles, Vec4Swizzles};

const EPSILON: f32 = 0.00001;
const GAUS_KERNEL: [f32; 9] = [
    1.0 / 16.0,
    1.0 / 8.0,
    1.0 / 16.0,
    1.0 / 8.0,
    1.0 / 4.0,
    1.0 / 8.0,
    1.0 / 16.0,
    1.0 / 8.0,
    1.0 / 16.0,
];

const WAVELET_H: [f32; 25] = [
    1.0 / 256.0,
    1.0 / 64.0,
    3.0 / 128.0,
    1.0 / 64.0,
    1.0 / 256.0,
    1.0 / 64.0,
    1.0 / 16.0,
    3.0 / 32.0,
    1.0 / 16.0,
    1.0 / 64.0,
    3.0 / 128.0,
    3.0 / 32.0,
    9.0 / 64.0,
    3.0 / 32.0,
    3.0 / 128.0,
    1.0 / 64.0,
    1.0 / 16.0,
    3.0 / 32.0,
    1.0 / 16.0,
    1.0 / 64.0,
    1.0 / 256.0,
    1.0 / 64.0,
    3.0 / 128.0,
    1.0 / 64.0,
    1.0 / 256.0,
];
///Samples the SpecularBuffer spacially rejecting samples that are
/// _too different_ based on the normal and position criteria
fn denoised_specular(
    gicache: &TypedBuffer<SpecularBuffer>,
    coord: UVec2,
    push: &shared::SpecularPush,
) -> Vec3 {
    let (mididx, is_valid) = SpecularBuffer::index_2d_to_linear(coord, push.resolution.into());
    if !is_valid {
        //Sampling outside does not work
        return Vec3::ZERO;
    }
    let mid_cell = gicache.cells[mididx];
    let local_variance = mid_cell.variance;
    let mid_luminance = shared::util::lumiosity(mid_cell.specular.into());

    //Load the middle cell, always, with max weight
    let mut res: Vec3 = Vec3::ZERO;
    let mut accweight = 0.0;
    const KERNEL_SIZE: i32 = 2;

    for x in -KERNEL_SIZE..=KERNEL_SIZE {
        for y in -KERNEL_SIZE..=KERNEL_SIZE {
            //Potential weight of the sample
            let sample_coord = coord.as_ivec2() + IVec2::new(x, y);
            let (sample_idx, is_sample_valid) =
                SpecularBuffer::index_2d_to_linear(sample_coord.as_uvec2(), push.resolution.into());
            if !is_sample_valid {
                continue;
            }

            //load sample
            let sample_cell = gicache.cells[sample_idx];

            let t = Vec3::from(mid_cell.pos) - Vec3::from(sample_cell.pos);
            let dist2 = t.dot(t);
            //weight of the postional component
            let wp = (-(dist2) / push.spec_deniose_sigma_pos).exp().min(1.0);
            //weight of the normal component.
            let wn = Vec3::from(sample_cell.normal)
                .dot(mid_cell.normal.into())
                .max(0.0)
                .powf(push.spec_denoise_sigma_normal);

            //TODO: prefilter with gaus3 instead of doing that here
            let mut gvl = 0.0001;
            for gy in -1..1 {
                for gx in -1..1 {
                    let gaus_cell_sample = sample_coord + IVec2::new(gx, gy);
                    let (gaus_cell_idx, is_gaus_valid) = SpecularBuffer::index_2d_to_linear(
                        gaus_cell_sample.as_uvec2(),
                        push.resolution.into(),
                    );
                    if !is_gaus_valid {
                        continue;
                    }

                    let lgv = gicache.cells[gaus_cell_idx].variance;
                    gvl += GAUS_KERNEL[(gx + 3 * gy + 4) as usize] * lgv;
                }
            }

            //After sampling the variance values, create the lumiosity rejection
            let local_luminance = shared::util::lumiosity(sample_cell.specular.into());
            let wl = (-((mid_luminance - local_luminance).abs())
                / (push.spec_denoise_sigma_luminance * gvl.sqrt() + EPSILON))
                .exp()
                .min(1.0);

            let w = wp * wn * wl;

            let weight = WAVELET_H[(5 * (y + KERNEL_SIZE) + x + KERNEL_SIZE) as usize] * w;

            res += Vec3::from(sample_cell.specular) * weight;
            accweight += weight;
        }
    }

    res / accweight
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn specular(
    #[spirv(push_constant)] push: &shared::SpecularPush,
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(descriptor_set = 1, binding = 0)] rgbaf32_images: &RuntimeArray<
        Image!(2D, format = rgba32f, sampled = false),
    >,
    #[spirv(descriptor_set = 0, binding = 0, storage_buffer)] spec_cache: &mut RuntimeArray<
        TypedBuffer<SpecularBuffer>,
    >,
) {
    if push.specular_cache.is_invalid() || push.depth_image.is_invalid() {
        return;
    }
    let coord = id.xy();
    if coord.x >= push.resolution[0] || coord.y >= push.resolution[1] {
        return;
    }
    let coordf32 = coord.as_vec2();
    let coord_uv = coordf32 / UVec2::new(push.resolution[0], push.resolution[1]).as_vec2();
    let depth = if push.depth_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.depth_image.index() as usize)
                .read(coord)
                .x
        }
    } else {
        0.0
    };

    let specular = denoised_specular(
        unsafe { spec_cache.index(push.specular_cache.index() as usize) },
        coord,
        push,
    );

    let specular = shared::util::default_camera_exposure() * specular;
    if push.target_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.target_image.index() as usize)
                .write(coord, specular.extend(depth));
        }
    }
}
