#![cfg_attr(target_arch = "spirv", no_std)]
#![feature(asm_experimental_arch)]

use core::f32::consts::PI;

use shared::glam::{vec3, UVec4, Vec2, Vec4, Vec4Swizzles};
use shared::sky::{fast_sky, DirLight};
use shared::spec_cache::{SpecularBuffer, SpecularCache};
use shared::util::{dir_hemi_sampled, random_dir_hemi, reflect, unit_vec};
use spirv_std::glam::{UVec2, UVec3, Vec3, Vec3Swizzles};
#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::Float;
use spirv_std::{self, Sampler};
use spirv_std::{spirv, Image, RuntimeArray, TypedBuffer};

const NUM_GI_TRACES: usize = 1;

pub fn map_bluenoise_sample(sample: UVec4) -> Vec4 {
    //map each channel down to 0..1
    sample.as_vec4() / Vec4::splat(u32::MAX as f32)
}

//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_sdf"]
pub fn eval_sdf(pos: Vec3, offset: Vec3, time: f32) -> f32 {
    let dist = (pos - offset).length() - (1.0 * time.sin());
    dist
}
//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_albedo"]
pub fn eval_albedo(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}
//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_mro"]
pub fn eval_mro(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}

//Uses the thetrahedron technique described here: https://iquilezles.org/articles/normalsSDF/
fn calc_normal(at: Vec3, offset: Vec3, time: f32) -> Vec3 {
    const H: f32 = 0.00001;
    (vec3(1.0, -1.0, -1.0) * eval_sdf(at + (H * vec3(1.0, -1.0, -1.0)), offset, time)
        + vec3(-1.0, -1.0, 1.0) * eval_sdf(at + (H * vec3(-1.0, -1.0, 1.0)), offset, time)
        + vec3(-1.0, 1.0, -1.0) * eval_sdf(at + (H * vec3(-1.0, 1.0, -1.0)), offset, time)
        + vec3(1.0, 1.0, 1.0) * eval_sdf(at + (H * vec3(1.0, 1.0, 1.0)), offset, time))
    .normalize()
}

struct HitInfo {
    albedo: Vec3,
    t: f32,
    nrm: Vec3,
}

fn trace_ray(ray: &shared::Ray, push: &shared::SpecularTracePush) -> HitInfo {
    //Short ray, at max 32 iterations
    let mut hit = HitInfo {
        albedo: Vec3::ZERO,
        t: f32::INFINITY,
        nrm: -Vec3::Y,
    };

    let mut t = 0.05;
    for _ in 0..32 {
        let r = eval_sdf(ray.at(t), push.offset.into(), push.time);

        if r < 0.01 {
            let albedo = eval_albedo(ray.at(t), push.offset.into(), push.time);
            let nrm = calc_normal(ray.at(t), push.offset.into(), push.time);
            hit = HitInfo { albedo, nrm, t };

            break;
        }

        t += r.max(0.05);
        if t > push.specular_max_trace {
            break;
        }
    }

    hit
}

fn trace_specular_light(
    mut ray: shared::Ray,
    push: &shared::SpecularTracePush,
    rnd: &mut u32,
) -> Vec3 {
    let mut ret = Vec3::ZERO;
    let mut throughput = Vec3::ONE;

    for _b in 0..push.bounces {
        let hit = trace_ray(&ray, push);
        if hit.t == f32::INFINITY {
            //add sky and exit
            ret += fast_sky(ray.direction, &DirLight::default());
            break;
        } else {
            //has hit, therfore update ray and result, then bounce
            let mut hitpos = ray.at(hit.t);
            //offset to avoid self intersection.
            hitpos += hit.nrm * 0.1;
            ray.origin = hitpos;
            ray.direction = random_dir_hemi(hit.nrm, rnd);
            //Update albedo and throughput
            ret += hit.albedo * throughput;
            throughput *= hit.albedo;
        }
    }

    ret
}

fn sample_noise(
    image: &Image!(3D, format = rgba32ui, sampled = true),
    sampler: &Sampler,
    coord: UVec2,
    idx: u32,
) -> Vec4 {
    let src_coord = (coord % UVec2::splat(64)).as_vec2() / Vec2::splat(64.0);
    //add index to coord
    let src_coord = src_coord.extend((idx % 64) as f32 / 64.0);
    //Now sample and return

    let src = image.sample_by_lod(*sampler, src_coord, 0.0);

    //is encoded in 32bit, but actually just 16bit.
    src.as_vec4() / Vec4::splat((1 << 18) as f32)
}

fn ggx_sample(normal: Vec3, view: Vec3, roughness: f32, rand: Vec4) -> (Vec3, f32) {
    let alpha = roughness * roughness;
    let alpha2 = alpha * alpha;

    let eps = rand.x.clamp(0.0001, 1.0);
    let costheta2 = (1.0 - eps) / (eps * (alpha2 - 1.0) + 1.0);
    let costheta = costheta2.sqrt();
    let sintheta = (1.0 - costheta2).sqrt();

    let phi = 2.0 * PI * rand.y;

    let t = (normal.yzx()).cross(normal);
    let b = normal.cross(t);
    let micro_normal = (t * phi.cos() + b * phi.sin()) * sintheta + normal * costheta;

    let l = reflect(-view, micro_normal);
    let den = (alpha2 - 1.0) * costheta2 + 1.0;
    let d = alpha2 / (PI * den * den);
    let pdf = d * costheta / (4.0 * micro_normal.dot(view));

    if l.dot(normal) < 0.0 {
        (l, 0.0)
    } else {
        (l, 1.0)
    }
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn specular_trace(
    #[spirv(push_constant)] push: &shared::SpecularTracePush,
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(descriptor_set = 1, binding = 0)] rgbaf32_images: &RuntimeArray<
        Image!(2D, format = rgba32f, sampled = false),
    >,
    #[spirv(descriptor_set = 2, binding = 0)] rgbau32_sampled: &RuntimeArray<
        Image!(3D, format = rgba32ui, sampled = true),
    >,
    #[spirv(descriptor_set = 0, binding = 0, storage_buffer)] specular_cache: &mut RuntimeArray<
        TypedBuffer<SpecularBuffer>,
    >,
    #[spirv(descriptor_set = 3, binding = 0)] sampler: &RuntimeArray<Sampler>,
) {
    if push.specular_cache.is_invalid()
        || push.bluenoise.is_invalid()
        || push.noise_sampler.is_invalid()
        || push.depth_image.is_invalid()
    {
        return;
    }
    let coord = id.xy();
    if coord.x >= push.resolution[0] || coord.y >= push.resolution[1] {
        return;
    }
    let coordf32 = coord.as_vec2();
    let coord_uv = coordf32 / UVec2::new(push.resolution[0], push.resolution[1]).as_vec2();

    //reconstruct the ray position from depth
    let ndc = coord_uv * 2.0 - 1.0;
    let ray = push.camera.ray_from_ndc(ndc, push.resolution);

    let depth = if push.depth_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.depth_image.index() as usize)
                .read(coord)
                .x
        }
    } else {
        0.0
    };

    let pos = ray.at(depth);

    let (cindex, is_valid) = SpecularBuffer::index_2d_to_linear(coord, push.resolution.into());

    if !is_valid {
        return;
    }

    //Calculate MRO and Albedo at this position.
    //we use that information to
    //launch the ray, and possibly tint the result
    let nrm = calc_normal(pos, push.offset.into(), push.time);
    let albedo = eval_albedo(pos, push.offset.into(), push.time);
    let mro = eval_mro(pos, push.offset.into(), push.time);

    let noise_sampler = unsafe { sampler.index(push.noise_sampler.index() as usize) };
    let src = unsafe { rgbau32_sampled.index(push.bluenoise.index() as usize) };
    let noise_epoch_shift = coord / UVec2::splat(64);
    let noise = sample_noise(src, noise_sampler, coord, push.epoch + noise_epoch_shift.x);

    let mut state = (noise.z * u32::MAX as f32) as u32;
    let (dir, weight) = ggx_sample(nrm, -ray.direction, mro.y, noise);

    let res = trace_specular_light(
        shared::Ray {
            origin: pos + nrm * 0.1,
            max_t: push.specular_max_trace,
            direction: dir,
        },
        &push,
        &mut state,
    );

    //Weight the result based on schlick
    let l_dot_h = (-ray.direction).dot(nrm).clamp(0.001, 1.0);
    let refl2 = 0.04 * 0.04;
    let f0 = (1.0 - mro.x) * refl2 * 0.16 + albedo * mro.x;
    let res = res * shared::pbr::f_schlick(l_dot_h, f0).clamp(Vec3::splat(0.01), Vec3::ONE);

    //load cell
    let mut cell: SpecularCache = unsafe {
        specular_cache
            .index_mut(push.specular_cache.index() as usize)
            .cells[cindex as usize]
    };

    let cell_distance = (Vec3::from(cell.pos) - pos).length();
    let is_old_valid = Vec3::from(cell.normal).dot(nrm) > push.trace_cache_normal_reject
        // && cell_distance < GiCacheDesc::cell_size(depth, push.camera.fov);
        && cell_distance < push.trace_cache_postion_reject
            && push.reset_specular == 0 && (cell.roughness - mro.y).abs() < push.specular_roughness_recject;

    if is_old_valid {
        let old_lumiosity = shared::util::lumiosity(cell.specular.into());
        let this_lumiosity = shared::util::lumiosity(res);

        let carry_over = (cell.roughness * push.specular_carry_epsilon).clamp(0.0, 1.0);
        //reuse old radiance value
        cell.specular = (Vec3::from(cell.specular) * carry_over + res * (1.0 - carry_over)).into();
        cell.variance = (old_lumiosity - this_lumiosity).abs();
    } else {
        //set new
        cell.normal = nrm.into();
        cell.pos = pos.into();
        cell.specular = res.into();
        //set variance to _normal_
        cell.variance = 1.0;
        cell.metalness = mro.x;
        cell.roughness = mro.y;
    }

    unsafe {
        specular_cache
            .index_mut(push.specular_cache.index() as usize)
            .cells[cindex as usize] = cell;
    };
}
